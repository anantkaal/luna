import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:luna/model.dart';
import 'package:luna/pages.dart';

class SleepConnect extends StatefulWidget {
  const SleepConnect({super.key});

  @override
  State<SleepConnect> createState() => _SleepConnectState();
}

class _SleepConnectState extends State<SleepConnect> {
  @override
  Widget build(BuildContext context) {
    List _devices = [
      {
        'title': 'Fitbit ',
        'icon': 'assets/icons/devices/Fitbit.png',
      },
      {
        'title': 'Apple Health',
        'icon': 'assets/icons/devices/applehealth.png',
      },
      {
        'title': 'Whoop',
        'icon': 'assets/icons/devices/whoop.png',
      },
      {
        'title': 'Oura',
        'icon': 'assets/icons/devices/oura.png',
      },
      {
        'title': 'Samsung',
        'icon': 'assets/icons/devices/samsung.png',
      },
      {
        'title': 'Apple Watch',
        'icon': 'assets/icons/devices/applewatch.png',
      },
      {
        'title': 'Garmin',
        'icon': 'assets/icons/devices/garmin.png',
      },
    ];
    return Scaffold(
      appBar: AppBar(
        backgroundColor: background,
        elevation: 0,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(
            Icons.arrow_back_ios_new,
            color: unaicon,
          ),
        ),
        leadingWidth: 30,
        centerTitle: false,
        title: Text(
          'Sleep Tracking',
          style: GoogleFonts.openSans(
            color: Color(0xFF303841),
            fontSize: 26,
            fontWeight: FontWeight.w700,
          ),
        ),
      ),
      body: Container(
        padding: EdgeInsets.all(20),
        child: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                height: 390,
                child: GridView.builder(
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 3,
                    mainAxisSpacing: 15,
                    crossAxisSpacing: 15,
                    childAspectRatio: 2 / 2,
                  ),
                  shrinkWrap: true,
                  itemCount: _devices.length,
                  physics: NeverScrollableScrollPhysics(),
                  itemBuilder: (context, index) => GestureDetector(
                    onTap: () {
                      // Navigator.push(
                      //   context,
                      //   MaterialPageRoute(
                      //     builder: (context) => _devices[index]['navigation'],
                      //   ),
                      // );
                    },
                    child: Container(
                      height: 100,
                      width: 100,
                      decoration: BoxDecoration(
                        color: box,
                        borderRadius: BorderRadius.circular(
                          8,
                        ),
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          SizedBox(
                            height: 50,
                            child: Image.asset(
                              _devices[index]['icon'],
                            ),
                          ),
                          Text(
                            _devices[index]["title"],
                            textAlign: TextAlign.center,
                            style: GoogleFonts.openSans(
                              color: unaicon,
                              fontSize: 14,
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              MainActiveButton(
                title: "SYNC",
                onTap: () {},
              ),
              SizedBox(
                height: 20,
              ),
              BorderedButton1(
                text: 'Don’t have a tracking device?',
                onpressed: () => Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => DonthaveDevice(),
                  ),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              JustTextButton(
                text: 'Sync Other Devices',
                onpressed: () => Navigator.of(context).pop(),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
