import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:luna/condevicespages/successfullsync.dart';
import 'package:luna/model.dart';

class DosageTimes extends StatefulWidget {
  DosageTimes({
    super.key,
    required this.nofdoses,
  });

  int nofdoses;

  @override
  State<DosageTimes> createState() => _DosageTimesState();
}

class _DosageTimesState extends State<DosageTimes> {
  bool isAM1 = true;
  bool isAM2 = true;
  bool isAM3 = true;
  bool isAM4 = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: background,
        elevation: 0,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(
            Icons.arrow_back_ios_new,
            color: unaicon,
          ),
        ),
        leadingWidth: 30,
        centerTitle: false,
        title: Text(
          "Dosage Times",
          style: TextStyle(
            color: unaicon,
            fontSize: 24,
            fontWeight: FontWeight.w700,
          ),
        ),
      ),
      body: Container(
        padding: EdgeInsets.all(20),
        child: SingleChildScrollView(
          child: Column(
            children: [
              (widget.nofdoses == 1)
                  ? Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Time of first dose",
                          style: GoogleFonts.openSans(
                            color: unaicon,
                            fontSize: 16,
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Row(
                          children: [
                            Container(
                              height: 48,
                              width: 67,
                              decoration: BoxDecoration(
                                color: Colors.white,
                                border: Border.all(
                                  color: border,
                                ),
                                borderRadius: BorderRadius.circular(8),
                              ),
                              child: TextFormField(
                                keyboardType: TextInputType.number,
                                validator: (value) {
                                  if (value == null) {
                                    return "this value shouldn't be empty";
                                  }
                                  final n = num.tryParse(value);
                                  if (60 <= n!) {
                                    return "should be less 60";
                                  }
                                  return null;
                                },
                                textAlign: TextAlign.center,
                                decoration: InputDecoration(
                                  border: InputBorder.none,
                                  hintText: "00",
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Container(
                              height: 48,
                              width: 67,
                              decoration: BoxDecoration(
                                color: Colors.white,
                                border: Border.all(
                                  color: border,
                                ),
                                borderRadius: BorderRadius.circular(8),
                              ),
                              child: TextFormField(
                                textAlign: TextAlign.center,
                                decoration: InputDecoration(
                                  border: InputBorder.none,
                                  hintText: "00",
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            //Toggle switch starts here
                            Container(
                              height: 48,
                              width: 120,
                              decoration: BoxDecoration(
                                color: box1,
                                borderRadius: BorderRadius.circular(8),
                              ),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  GestureDetector(
                                    onTap: () {
                                      setState(() {
                                        isAM1 = true;
                                      });
                                    },
                                    child: Container(
                                      width: 37,
                                      height: 28,
                                      alignment: Alignment.center,
                                      decoration: BoxDecoration(
                                        color:
                                            isAM1 ? purple : Colors.transparent,
                                        borderRadius: BorderRadius.circular(8),
                                      ),
                                      child: Text(
                                        "AM",
                                        maxLines: 1,
                                        textAlign: TextAlign.center,
                                        style: GoogleFonts.openSans(
                                          fontSize: 16,
                                          fontWeight: FontWeight.w400,
                                          color: isAM1 ? Colors.white : unaicon,
                                        ),
                                      ),
                                    ),
                                  ),
                                  GestureDetector(
                                    onTap: () {
                                      setState(() {
                                        isAM1 = false;
                                      });
                                    },
                                    child: Container(
                                      width: 37,
                                      height: 28,
                                      alignment: Alignment.center,
                                      decoration: BoxDecoration(
                                        color:
                                            isAM1 ? Colors.transparent : purple,
                                        borderRadius: BorderRadius.circular(8),
                                      ),
                                      child: Text(
                                        textAlign: TextAlign.center,
                                        maxLines: 1,
                                        "PM",
                                        style: GoogleFonts.openSans(
                                          fontSize: 16,
                                          fontWeight: FontWeight.w400,
                                          color: isAM1 ? unaicon : Colors.white,
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            //Toggle switch ends here
                          ],
                        )
                      ],
                    )
                  : (widget.nofdoses == 2)
                      ? Column(
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Time of first dose",
                                  style: GoogleFonts.openSans(
                                    color: unaicon,
                                    fontSize: 16,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                                SizedBox(
                                  height: 5,
                                ),
                                Row(
                                  children: [
                                    Container(
                                      height: 48,
                                      width: 67,
                                      decoration: BoxDecoration(
                                        color: Colors.white,
                                        border: Border.all(
                                          color: border,
                                        ),
                                        borderRadius: BorderRadius.circular(8),
                                      ),
                                      child: TextFormField(
                                        keyboardType: TextInputType.number,
                                        validator: (value) {
                                          if (value == null) {
                                            return "this value shouldn't be empty";
                                          }
                                          final n = num.tryParse(value);
                                          if (60 <= n!) {
                                            return "should be less 60";
                                          }
                                          return null;
                                        },
                                        textAlign: TextAlign.center,
                                        decoration: InputDecoration(
                                          border: InputBorder.none,
                                          hintText: "00",
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    Container(
                                      height: 48,
                                      width: 67,
                                      decoration: BoxDecoration(
                                        color: Colors.white,
                                        border: Border.all(
                                          color: border,
                                        ),
                                        borderRadius: BorderRadius.circular(8),
                                      ),
                                      child: TextFormField(
                                        textAlign: TextAlign.center,
                                        decoration: InputDecoration(
                                          border: InputBorder.none,
                                          hintText: "00",
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    //Toggle switch starts here
                                    Container(
                                      height: 48,
                                      width: 120,
                                      decoration: BoxDecoration(
                                        color: box1,
                                        borderRadius: BorderRadius.circular(8),
                                      ),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceEvenly,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          GestureDetector(
                                            onTap: () {
                                              setState(() {
                                                isAM1 = true;
                                              });
                                            },
                                            child: Container(
                                              width: 37,
                                              height: 28,
                                              alignment: Alignment.center,
                                              decoration: BoxDecoration(
                                                color: isAM1
                                                    ? purple
                                                    : Colors.transparent,
                                                borderRadius:
                                                    BorderRadius.circular(8),
                                              ),
                                              child: Text(
                                                "AM",
                                                maxLines: 1,
                                                textAlign: TextAlign.center,
                                                style: GoogleFonts.openSans(
                                                  fontSize: 16,
                                                  fontWeight: FontWeight.w400,
                                                  color: isAM1
                                                      ? Colors.white
                                                      : unaicon,
                                                ),
                                              ),
                                            ),
                                          ),
                                          GestureDetector(
                                            onTap: () {
                                              setState(() {
                                                isAM1 = false;
                                              });
                                            },
                                            child: Container(
                                              width: 37,
                                              height: 28,
                                              alignment: Alignment.center,
                                              decoration: BoxDecoration(
                                                color: isAM1
                                                    ? Colors.transparent
                                                    : purple,
                                                borderRadius:
                                                    BorderRadius.circular(8),
                                              ),
                                              child: Text(
                                                textAlign: TextAlign.center,
                                                maxLines: 1,
                                                "PM",
                                                style: GoogleFonts.openSans(
                                                  fontSize: 16,
                                                  fontWeight: FontWeight.w400,
                                                  color: isAM1
                                                      ? unaicon
                                                      : Colors.white,
                                                ),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    //Toggle switch ends here
                                  ],
                                )
                              ],
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Time of second dose",
                                  style: GoogleFonts.openSans(
                                    color: unaicon,
                                    fontSize: 16,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                                SizedBox(
                                  height: 5,
                                ),
                                Row(
                                  children: [
                                    Container(
                                      height: 48,
                                      width: 67,
                                      decoration: BoxDecoration(
                                        color: Colors.white,
                                        border: Border.all(
                                          color: border,
                                        ),
                                        borderRadius: BorderRadius.circular(8),
                                      ),
                                      child: TextFormField(
                                        keyboardType: TextInputType.number,
                                        validator: (value) {
                                          if (value == null) {
                                            return "this value shouldn't be empty";
                                          }
                                          final n = num.tryParse(value);
                                          if (60 <= n!) {
                                            return "should be less 60";
                                          }
                                          return null;
                                        },
                                        textAlign: TextAlign.center,
                                        decoration: InputDecoration(
                                          border: InputBorder.none,
                                          hintText: "00",
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    Container(
                                      height: 48,
                                      width: 67,
                                      decoration: BoxDecoration(
                                        color: Colors.white,
                                        border: Border.all(
                                          color: border,
                                        ),
                                        borderRadius: BorderRadius.circular(8),
                                      ),
                                      child: TextFormField(
                                        textAlign: TextAlign.center,
                                        decoration: InputDecoration(
                                          border: InputBorder.none,
                                          hintText: "00",
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    //Toggle switch starts here
                                    Container(
                                      height: 48,
                                      width: 120,
                                      decoration: BoxDecoration(
                                        color: box1,
                                        borderRadius: BorderRadius.circular(8),
                                      ),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceEvenly,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          GestureDetector(
                                            onTap: () {
                                              setState(() {
                                                isAM2 = true;
                                              });
                                            },
                                            child: Container(
                                              width: 37,
                                              height: 28,
                                              alignment: Alignment.center,
                                              decoration: BoxDecoration(
                                                color: isAM2
                                                    ? purple
                                                    : Colors.transparent,
                                                borderRadius:
                                                    BorderRadius.circular(8),
                                              ),
                                              child: Text(
                                                "AM",
                                                maxLines: 1,
                                                textAlign: TextAlign.center,
                                                style: GoogleFonts.openSans(
                                                  fontSize: 16,
                                                  fontWeight: FontWeight.w400,
                                                  color: isAM2
                                                      ? Colors.white
                                                      : unaicon,
                                                ),
                                              ),
                                            ),
                                          ),
                                          GestureDetector(
                                            onTap: () {
                                              setState(() {
                                                isAM2 = false;
                                              });
                                            },
                                            child: Container(
                                              width: 37,
                                              height: 28,
                                              alignment: Alignment.center,
                                              decoration: BoxDecoration(
                                                color: isAM2
                                                    ? Colors.transparent
                                                    : purple,
                                                borderRadius:
                                                    BorderRadius.circular(8),
                                              ),
                                              child: Text(
                                                textAlign: TextAlign.center,
                                                maxLines: 1,
                                                "PM",
                                                style: GoogleFonts.openSans(
                                                  fontSize: 16,
                                                  fontWeight: FontWeight.w400,
                                                  color: isAM2
                                                      ? unaicon
                                                      : Colors.white,
                                                ),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    //Toggle switch ends here
                                  ],
                                )
                              ],
                            )
                          ],
                        )
                      : (widget.nofdoses == 3)
                          ? Column(
                              children: [
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      "Time of first dose",
                                      style: GoogleFonts.openSans(
                                        color: unaicon,
                                        fontSize: 16,
                                        fontWeight: FontWeight.w400,
                                      ),
                                    ),
                                    SizedBox(
                                      height: 5,
                                    ),
                                    Row(
                                      children: [
                                        Container(
                                          height: 48,
                                          width: 67,
                                          decoration: BoxDecoration(
                                            color: Colors.white,
                                            border: Border.all(
                                              color: border,
                                            ),
                                            borderRadius:
                                                BorderRadius.circular(8),
                                          ),
                                          child: TextFormField(
                                            keyboardType: TextInputType.number,
                                            validator: (value) {
                                              if (value == null) {
                                                return "this value shouldn't be empty";
                                              }
                                              final n = num.tryParse(value);
                                              if (60 <= n!) {
                                                return "should be less 60";
                                              }
                                              return null;
                                            },
                                            textAlign: TextAlign.center,
                                            decoration: InputDecoration(
                                              border: InputBorder.none,
                                              hintText: "00",
                                            ),
                                          ),
                                        ),
                                        SizedBox(
                                          width: 10,
                                        ),
                                        Container(
                                          height: 48,
                                          width: 67,
                                          decoration: BoxDecoration(
                                            color: Colors.white,
                                            border: Border.all(
                                              color: border,
                                            ),
                                            borderRadius:
                                                BorderRadius.circular(8),
                                          ),
                                          child: TextFormField(
                                            textAlign: TextAlign.center,
                                            decoration: InputDecoration(
                                              border: InputBorder.none,
                                              hintText: "00",
                                            ),
                                          ),
                                        ),
                                        SizedBox(
                                          width: 10,
                                        ),
                                        //Toggle switch starts here
                                        Container(
                                          height: 48,
                                          width: 120,
                                          decoration: BoxDecoration(
                                            color: box1,
                                            borderRadius:
                                                BorderRadius.circular(8),
                                          ),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceEvenly,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            children: [
                                              GestureDetector(
                                                onTap: () {
                                                  setState(() {
                                                    isAM1 = true;
                                                  });
                                                },
                                                child: Container(
                                                  width: 37,
                                                  height: 28,
                                                  alignment: Alignment.center,
                                                  decoration: BoxDecoration(
                                                    color: isAM1
                                                        ? purple
                                                        : Colors.transparent,
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            8),
                                                  ),
                                                  child: Text(
                                                    "AM",
                                                    maxLines: 1,
                                                    textAlign: TextAlign.center,
                                                    style: GoogleFonts.openSans(
                                                      fontSize: 16,
                                                      fontWeight:
                                                          FontWeight.w400,
                                                      color: isAM1
                                                          ? Colors.white
                                                          : unaicon,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              GestureDetector(
                                                onTap: () {
                                                  setState(() {
                                                    isAM3 = false;
                                                  });
                                                },
                                                child: Container(
                                                  width: 37,
                                                  height: 28,
                                                  alignment: Alignment.center,
                                                  decoration: BoxDecoration(
                                                    color: isAM1
                                                        ? Colors.transparent
                                                        : purple,
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            8),
                                                  ),
                                                  child: Text(
                                                    textAlign: TextAlign.center,
                                                    maxLines: 1,
                                                    "PM",
                                                    style: GoogleFonts.openSans(
                                                      fontSize: 16,
                                                      fontWeight:
                                                          FontWeight.w400,
                                                      color: isAM1
                                                          ? unaicon
                                                          : Colors.white,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        //Toggle switch ends here
                                      ],
                                    )
                                  ],
                                ),
                                SizedBox(
                                  height: 20,
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      "Time of second dose",
                                      style: GoogleFonts.openSans(
                                        color: unaicon,
                                        fontSize: 16,
                                        fontWeight: FontWeight.w400,
                                      ),
                                    ),
                                    SizedBox(
                                      height: 5,
                                    ),
                                    Row(
                                      children: [
                                        Container(
                                          height: 48,
                                          width: 67,
                                          decoration: BoxDecoration(
                                            color: Colors.white,
                                            border: Border.all(
                                              color: border,
                                            ),
                                            borderRadius:
                                                BorderRadius.circular(8),
                                          ),
                                          child: TextFormField(
                                            keyboardType: TextInputType.number,
                                            validator: (value) {
                                              if (value == null) {
                                                return "this value shouldn't be empty";
                                              }
                                              final n = num.tryParse(value);
                                              if (60 <= n!) {
                                                return "should be less 60";
                                              }
                                              return null;
                                            },
                                            textAlign: TextAlign.center,
                                            decoration: InputDecoration(
                                              border: InputBorder.none,
                                              hintText: "00",
                                            ),
                                          ),
                                        ),
                                        SizedBox(
                                          width: 10,
                                        ),
                                        Container(
                                          height: 48,
                                          width: 67,
                                          decoration: BoxDecoration(
                                            color: Colors.white,
                                            border: Border.all(
                                              color: border,
                                            ),
                                            borderRadius:
                                                BorderRadius.circular(8),
                                          ),
                                          child: TextFormField(
                                            textAlign: TextAlign.center,
                                            decoration: InputDecoration(
                                              border: InputBorder.none,
                                              hintText: "00",
                                            ),
                                          ),
                                        ),
                                        SizedBox(
                                          width: 10,
                                        ),
                                        //Toggle switch starts here
                                        Container(
                                          height: 48,
                                          width: 120,
                                          decoration: BoxDecoration(
                                            color: box1,
                                            borderRadius:
                                                BorderRadius.circular(8),
                                          ),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceEvenly,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            children: [
                                              GestureDetector(
                                                onTap: () {
                                                  setState(() {
                                                    isAM2 = true;
                                                  });
                                                },
                                                child: Container(
                                                  width: 37,
                                                  height: 28,
                                                  alignment: Alignment.center,
                                                  decoration: BoxDecoration(
                                                    color: isAM2
                                                        ? purple
                                                        : Colors.transparent,
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            8),
                                                  ),
                                                  child: Text(
                                                    "AM",
                                                    maxLines: 1,
                                                    textAlign: TextAlign.center,
                                                    style: GoogleFonts.openSans(
                                                      fontSize: 16,
                                                      fontWeight:
                                                          FontWeight.w400,
                                                      color: isAM2
                                                          ? Colors.white
                                                          : unaicon,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              GestureDetector(
                                                onTap: () {
                                                  setState(() {
                                                    isAM2 = false;
                                                  });
                                                },
                                                child: Container(
                                                  width: 37,
                                                  height: 28,
                                                  alignment: Alignment.center,
                                                  decoration: BoxDecoration(
                                                    color: isAM2
                                                        ? Colors.transparent
                                                        : purple,
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            8),
                                                  ),
                                                  child: Text(
                                                    textAlign: TextAlign.center,
                                                    maxLines: 1,
                                                    "PM",
                                                    style: GoogleFonts.openSans(
                                                      fontSize: 16,
                                                      fontWeight:
                                                          FontWeight.w400,
                                                      color: isAM2
                                                          ? unaicon
                                                          : Colors.white,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        //Toggle switch ends here
                                      ],
                                    )
                                  ],
                                ),
                                SizedBox(
                                  height: 20,
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      "Time of third dose",
                                      style: GoogleFonts.openSans(
                                        color: unaicon,
                                        fontSize: 16,
                                        fontWeight: FontWeight.w400,
                                      ),
                                    ),
                                    SizedBox(
                                      height: 5,
                                    ),
                                    Row(
                                      children: [
                                        Container(
                                          height: 48,
                                          width: 67,
                                          decoration: BoxDecoration(
                                            color: Colors.white,
                                            border: Border.all(
                                              color: border,
                                            ),
                                            borderRadius:
                                                BorderRadius.circular(8),
                                          ),
                                          child: TextFormField(
                                            keyboardType: TextInputType.number,
                                            validator: (value) {
                                              if (value == null) {
                                                return "this value shouldn't be empty";
                                              }
                                              final n = num.tryParse(value);
                                              if (60 <= n!) {
                                                return "should be less 60";
                                              }
                                              return null;
                                            },
                                            textAlign: TextAlign.center,
                                            decoration: InputDecoration(
                                              border: InputBorder.none,
                                              hintText: "00",
                                            ),
                                          ),
                                        ),
                                        SizedBox(
                                          width: 10,
                                        ),
                                        Container(
                                          height: 48,
                                          width: 67,
                                          decoration: BoxDecoration(
                                            color: Colors.white,
                                            border: Border.all(
                                              color: border,
                                            ),
                                            borderRadius:
                                                BorderRadius.circular(8),
                                          ),
                                          child: TextFormField(
                                            textAlign: TextAlign.center,
                                            decoration: InputDecoration(
                                              border: InputBorder.none,
                                              hintText: "00",
                                            ),
                                          ),
                                        ),
                                        SizedBox(
                                          width: 10,
                                        ),
                                        //Toggle switch starts here
                                        Container(
                                          height: 48,
                                          width: 120,
                                          decoration: BoxDecoration(
                                            color: box1,
                                            borderRadius:
                                                BorderRadius.circular(8),
                                          ),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceEvenly,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            children: [
                                              GestureDetector(
                                                onTap: () {
                                                  setState(() {
                                                    isAM3 = true;
                                                  });
                                                },
                                                child: Container(
                                                  width: 37,
                                                  height: 28,
                                                  alignment: Alignment.center,
                                                  decoration: BoxDecoration(
                                                    color: isAM3
                                                        ? purple
                                                        : Colors.transparent,
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            8),
                                                  ),
                                                  child: Text(
                                                    "AM",
                                                    maxLines: 1,
                                                    textAlign: TextAlign.center,
                                                    style: GoogleFonts.openSans(
                                                      fontSize: 16,
                                                      fontWeight:
                                                          FontWeight.w400,
                                                      color: isAM3
                                                          ? Colors.white
                                                          : unaicon,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              GestureDetector(
                                                onTap: () {
                                                  setState(() {
                                                    isAM3 = false;
                                                  });
                                                },
                                                child: Container(
                                                  width: 37,
                                                  height: 28,
                                                  alignment: Alignment.center,
                                                  decoration: BoxDecoration(
                                                    color: isAM3
                                                        ? Colors.transparent
                                                        : purple,
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            8),
                                                  ),
                                                  child: Text(
                                                    textAlign: TextAlign.center,
                                                    maxLines: 1,
                                                    "PM",
                                                    style: GoogleFonts.openSans(
                                                      fontSize: 16,
                                                      fontWeight:
                                                          FontWeight.w400,
                                                      color: isAM3
                                                          ? unaicon
                                                          : Colors.white,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        //Toggle switch ends here
                                      ],
                                    )
                                  ],
                                )
                              ],
                            )
                          : (widget.nofdoses == 4)
                              ? Column(
                                  children: [
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          "Time of first dose",
                                          style: GoogleFonts.openSans(
                                            color: unaicon,
                                            fontSize: 16,
                                            fontWeight: FontWeight.w400,
                                          ),
                                        ),
                                        SizedBox(
                                          height: 5,
                                        ),
                                        Row(
                                          children: [
                                            Container(
                                              height: 48,
                                              width: 67,
                                              decoration: BoxDecoration(
                                                color: Colors.white,
                                                border: Border.all(
                                                  color: border,
                                                ),
                                                borderRadius:
                                                    BorderRadius.circular(8),
                                              ),
                                              child: TextFormField(
                                                keyboardType:
                                                    TextInputType.number,
                                                validator: (value) {
                                                  if (value == null) {
                                                    return "this value shouldn't be empty";
                                                  }
                                                  final n = num.tryParse(value);
                                                  if (60 <= n!) {
                                                    return "should be less 60";
                                                  }
                                                  return null;
                                                },
                                                textAlign: TextAlign.center,
                                                decoration: InputDecoration(
                                                  border: InputBorder.none,
                                                  hintText: "00",
                                                ),
                                              ),
                                            ),
                                            SizedBox(
                                              width: 10,
                                            ),
                                            Container(
                                              height: 48,
                                              width: 67,
                                              decoration: BoxDecoration(
                                                color: Colors.white,
                                                border: Border.all(
                                                  color: border,
                                                ),
                                                borderRadius:
                                                    BorderRadius.circular(8),
                                              ),
                                              child: TextFormField(
                                                textAlign: TextAlign.center,
                                                decoration: InputDecoration(
                                                  border: InputBorder.none,
                                                  hintText: "00",
                                                ),
                                              ),
                                            ),
                                            SizedBox(
                                              width: 10,
                                            ),
                                            //Toggle switch starts here
                                            Container(
                                              height: 48,
                                              width: 120,
                                              decoration: BoxDecoration(
                                                color: box1,
                                                borderRadius:
                                                    BorderRadius.circular(8),
                                              ),
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceEvenly,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.center,
                                                children: [
                                                  GestureDetector(
                                                    onTap: () {
                                                      setState(() {
                                                        isAM1 = true;
                                                      });
                                                    },
                                                    child: Container(
                                                      width: 37,
                                                      height: 28,
                                                      alignment:
                                                          Alignment.center,
                                                      decoration: BoxDecoration(
                                                        color: isAM1
                                                            ? purple
                                                            : Colors
                                                                .transparent,
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(8),
                                                      ),
                                                      child: Text(
                                                        "AM",
                                                        maxLines: 1,
                                                        textAlign:
                                                            TextAlign.center,
                                                        style: GoogleFonts
                                                            .openSans(
                                                          fontSize: 16,
                                                          fontWeight:
                                                              FontWeight.w400,
                                                          color: isAM1
                                                              ? Colors.white
                                                              : unaicon,
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                  GestureDetector(
                                                    onTap: () {
                                                      setState(() {
                                                        isAM1 = false;
                                                      });
                                                    },
                                                    child: Container(
                                                      width: 37,
                                                      height: 28,
                                                      alignment:
                                                          Alignment.center,
                                                      decoration: BoxDecoration(
                                                        color: isAM1
                                                            ? Colors.transparent
                                                            : purple,
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(8),
                                                      ),
                                                      child: Text(
                                                        textAlign:
                                                            TextAlign.center,
                                                        maxLines: 1,
                                                        "PM",
                                                        style: GoogleFonts
                                                            .openSans(
                                                          fontSize: 16,
                                                          fontWeight:
                                                              FontWeight.w400,
                                                          color: isAM1
                                                              ? unaicon
                                                              : Colors.white,
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            //Toggle switch ends here
                                          ],
                                        )
                                      ],
                                    ),
                                    SizedBox(
                                      height: 20,
                                    ),
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          "Time of second dose",
                                          style: GoogleFonts.openSans(
                                            color: unaicon,
                                            fontSize: 16,
                                            fontWeight: FontWeight.w400,
                                          ),
                                        ),
                                        SizedBox(
                                          height: 5,
                                        ),
                                        Row(
                                          children: [
                                            Container(
                                              height: 48,
                                              width: 67,
                                              decoration: BoxDecoration(
                                                color: Colors.white,
                                                border: Border.all(
                                                  color: border,
                                                ),
                                                borderRadius:
                                                    BorderRadius.circular(8),
                                              ),
                                              child: TextFormField(
                                                keyboardType:
                                                    TextInputType.number,
                                                validator: (value) {
                                                  if (value == null) {
                                                    return "this value shouldn't be empty";
                                                  }
                                                  final n = num.tryParse(value);
                                                  if (60 <= n!) {
                                                    return "should be less 60";
                                                  }
                                                  return null;
                                                },
                                                textAlign: TextAlign.center,
                                                decoration: InputDecoration(
                                                  border: InputBorder.none,
                                                  hintText: "00",
                                                ),
                                              ),
                                            ),
                                            SizedBox(
                                              width: 10,
                                            ),
                                            Container(
                                              height: 48,
                                              width: 67,
                                              decoration: BoxDecoration(
                                                color: Colors.white,
                                                border: Border.all(
                                                  color: border,
                                                ),
                                                borderRadius:
                                                    BorderRadius.circular(8),
                                              ),
                                              child: TextFormField(
                                                textAlign: TextAlign.center,
                                                decoration: InputDecoration(
                                                  border: InputBorder.none,
                                                  hintText: "00",
                                                ),
                                              ),
                                            ),
                                            SizedBox(
                                              width: 10,
                                            ),
                                            //Toggle switch starts here
                                            Container(
                                              height: 48,
                                              width: 120,
                                              decoration: BoxDecoration(
                                                color: box1,
                                                borderRadius:
                                                    BorderRadius.circular(8),
                                              ),
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceEvenly,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.center,
                                                children: [
                                                  GestureDetector(
                                                    onTap: () {
                                                      setState(() {
                                                        isAM2 = true;
                                                      });
                                                    },
                                                    child: Container(
                                                      width: 37,
                                                      height: 28,
                                                      alignment:
                                                          Alignment.center,
                                                      decoration: BoxDecoration(
                                                        color: isAM2
                                                            ? purple
                                                            : Colors
                                                                .transparent,
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(8),
                                                      ),
                                                      child: Text(
                                                        "AM",
                                                        maxLines: 1,
                                                        textAlign:
                                                            TextAlign.center,
                                                        style: GoogleFonts
                                                            .openSans(
                                                          fontSize: 16,
                                                          fontWeight:
                                                              FontWeight.w400,
                                                          color: isAM2
                                                              ? Colors.white
                                                              : unaicon,
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                  GestureDetector(
                                                    onTap: () {
                                                      setState(() {
                                                        isAM2 = false;
                                                      });
                                                    },
                                                    child: Container(
                                                      width: 37,
                                                      height: 28,
                                                      alignment:
                                                          Alignment.center,
                                                      decoration: BoxDecoration(
                                                        color: isAM2
                                                            ? Colors.transparent
                                                            : purple,
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(8),
                                                      ),
                                                      child: Text(
                                                        textAlign:
                                                            TextAlign.center,
                                                        maxLines: 1,
                                                        "PM",
                                                        style: GoogleFonts
                                                            .openSans(
                                                          fontSize: 16,
                                                          fontWeight:
                                                              FontWeight.w400,
                                                          color: isAM2
                                                              ? unaicon
                                                              : Colors.white,
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            //Toggle switch ends here
                                          ],
                                        )
                                      ],
                                    ),
                                    SizedBox(
                                      height: 20,
                                    ),
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          "Time of third dose",
                                          style: GoogleFonts.openSans(
                                            color: unaicon,
                                            fontSize: 16,
                                            fontWeight: FontWeight.w400,
                                          ),
                                        ),
                                        SizedBox(
                                          height: 5,
                                        ),
                                        Row(
                                          children: [
                                            Container(
                                              height: 48,
                                              width: 67,
                                              decoration: BoxDecoration(
                                                color: Colors.white,
                                                border: Border.all(
                                                  color: border,
                                                ),
                                                borderRadius:
                                                    BorderRadius.circular(8),
                                              ),
                                              child: TextFormField(
                                                keyboardType:
                                                    TextInputType.number,
                                                validator: (value) {
                                                  if (value == null) {
                                                    return "this value shouldn't be empty";
                                                  }
                                                  final n = num.tryParse(value);
                                                  if (60 <= n!) {
                                                    return "should be less 60";
                                                  }
                                                  return null;
                                                },
                                                textAlign: TextAlign.center,
                                                decoration: InputDecoration(
                                                  border: InputBorder.none,
                                                  hintText: "00",
                                                ),
                                              ),
                                            ),
                                            SizedBox(
                                              width: 10,
                                            ),
                                            Container(
                                              height: 48,
                                              width: 67,
                                              decoration: BoxDecoration(
                                                color: Colors.white,
                                                border: Border.all(
                                                  color: border,
                                                ),
                                                borderRadius:
                                                    BorderRadius.circular(8),
                                              ),
                                              child: TextFormField(
                                                textAlign: TextAlign.center,
                                                decoration: InputDecoration(
                                                  border: InputBorder.none,
                                                  hintText: "00",
                                                ),
                                              ),
                                            ),
                                            SizedBox(
                                              width: 10,
                                            ),
                                            //Toggle switch starts here
                                            Container(
                                              height: 48,
                                              width: 120,
                                              decoration: BoxDecoration(
                                                color: box1,
                                                borderRadius:
                                                    BorderRadius.circular(8),
                                              ),
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceEvenly,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.center,
                                                children: [
                                                  GestureDetector(
                                                    onTap: () {
                                                      setState(() {
                                                        isAM3 = true;
                                                      });
                                                    },
                                                    child: Container(
                                                      width: 37,
                                                      height: 28,
                                                      alignment:
                                                          Alignment.center,
                                                      decoration: BoxDecoration(
                                                        color: isAM3
                                                            ? purple
                                                            : Colors
                                                                .transparent,
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(8),
                                                      ),
                                                      child: Text(
                                                        "AM",
                                                        maxLines: 1,
                                                        textAlign:
                                                            TextAlign.center,
                                                        style: GoogleFonts
                                                            .openSans(
                                                          fontSize: 16,
                                                          fontWeight:
                                                              FontWeight.w400,
                                                          color: isAM3
                                                              ? Colors.white
                                                              : unaicon,
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                  GestureDetector(
                                                    onTap: () {
                                                      setState(() {
                                                        isAM3 = false;
                                                      });
                                                    },
                                                    child: Container(
                                                      width: 37,
                                                      height: 28,
                                                      alignment:
                                                          Alignment.center,
                                                      decoration: BoxDecoration(
                                                        color: isAM3
                                                            ? Colors.transparent
                                                            : purple,
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(8),
                                                      ),
                                                      child: Text(
                                                        textAlign:
                                                            TextAlign.center,
                                                        maxLines: 1,
                                                        "PM",
                                                        style: GoogleFonts
                                                            .openSans(
                                                          fontSize: 16,
                                                          fontWeight:
                                                              FontWeight.w400,
                                                          color: isAM3
                                                              ? unaicon
                                                              : Colors.white,
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            //Toggle switch ends here
                                          ],
                                        )
                                      ],
                                    ),
                                    SizedBox(
                                      height: 20,
                                    ),
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          "Time of fourth dose",
                                          style: GoogleFonts.openSans(
                                            color: unaicon,
                                            fontSize: 16,
                                            fontWeight: FontWeight.w400,
                                          ),
                                        ),
                                        SizedBox(
                                          height: 5,
                                        ),
                                        Row(
                                          children: [
                                            Container(
                                              height: 48,
                                              width: 67,
                                              decoration: BoxDecoration(
                                                color: Colors.white,
                                                border: Border.all(
                                                  color: border,
                                                ),
                                                borderRadius:
                                                    BorderRadius.circular(8),
                                              ),
                                              child: TextFormField(
                                                keyboardType:
                                                    TextInputType.number,
                                                validator: (value) {
                                                  if (value == null) {
                                                    return "this value shouldn't be empty";
                                                  }
                                                  final n = num.tryParse(value);
                                                  if (60 <= n!) {
                                                    return "should be less 60";
                                                  }
                                                  return null;
                                                },
                                                textAlign: TextAlign.center,
                                                decoration: InputDecoration(
                                                  border: InputBorder.none,
                                                  hintText: "00",
                                                ),
                                              ),
                                            ),
                                            SizedBox(
                                              width: 10,
                                            ),
                                            Container(
                                              height: 48,
                                              width: 67,
                                              decoration: BoxDecoration(
                                                color: Colors.white,
                                                border: Border.all(
                                                  color: border,
                                                ),
                                                borderRadius:
                                                    BorderRadius.circular(8),
                                              ),
                                              child: TextFormField(
                                                textAlign: TextAlign.center,
                                                decoration: InputDecoration(
                                                  border: InputBorder.none,
                                                  hintText: "00",
                                                ),
                                              ),
                                            ),
                                            SizedBox(
                                              width: 10,
                                            ),
                                            //Toggle switch starts here
                                            Container(
                                              height: 48,
                                              width: 120,
                                              decoration: BoxDecoration(
                                                color: box1,
                                                borderRadius:
                                                    BorderRadius.circular(8),
                                              ),
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceEvenly,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.center,
                                                children: [
                                                  GestureDetector(
                                                    onTap: () {
                                                      setState(() {
                                                        isAM4 = true;
                                                      });
                                                    },
                                                    child: Container(
                                                      width: 37,
                                                      height: 28,
                                                      alignment:
                                                          Alignment.center,
                                                      decoration: BoxDecoration(
                                                        color: isAM4
                                                            ? purple
                                                            : Colors
                                                                .transparent,
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(8),
                                                      ),
                                                      child: Text(
                                                        "AM",
                                                        maxLines: 1,
                                                        textAlign:
                                                            TextAlign.center,
                                                        style: GoogleFonts
                                                            .openSans(
                                                          fontSize: 16,
                                                          fontWeight:
                                                              FontWeight.w400,
                                                          color: isAM4
                                                              ? Colors.white
                                                              : unaicon,
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                  GestureDetector(
                                                    onTap: () {
                                                      setState(() {
                                                        isAM4 = false;
                                                      });
                                                    },
                                                    child: Container(
                                                      width: 37,
                                                      height: 28,
                                                      alignment:
                                                          Alignment.center,
                                                      decoration: BoxDecoration(
                                                        color: isAM4
                                                            ? Colors.transparent
                                                            : purple,
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(8),
                                                      ),
                                                      child: Text(
                                                        textAlign:
                                                            TextAlign.center,
                                                        maxLines: 1,
                                                        "PM",
                                                        style: GoogleFonts
                                                            .openSans(
                                                          fontSize: 16,
                                                          fontWeight:
                                                              FontWeight.w400,
                                                          color: isAM4
                                                              ? unaicon
                                                              : Colors.white,
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            //Toggle switch ends here
                                          ],
                                        )
                                      ],
                                    )
                                  ],
                                )
                              : Container(),
              SizedBox(
                height: 20,
              ),
              Container(
                width: double.infinity,
                height: 56,
                padding: EdgeInsets.all(10),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(
                      8,
                    ),
                    color: aicon),
                child: ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all(
                      aicon,
                    ),
                    elevation: MaterialStateProperty.all(0),
                  ),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => SyncSuccess(),
                      ),
                    );
                  },
                  child: Text(
                    'SAVE',
                    style: GoogleFonts.openSans(
                      color: Color(0xFF303841),
                      fontSize: 20,
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Container(
                width: double.infinity,
                height: 30,
                alignment: Alignment.center,
                child: Text(
                  'Add Another Medication',
                  textAlign: TextAlign.center,
                  style: GoogleFonts.openSans(
                    color: purple,
                    fontSize: 16,
                    fontWeight: FontWeight.w700,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
