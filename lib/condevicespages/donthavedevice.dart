import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:luna/model.dart';

class DonthaveDevice extends StatefulWidget {
  const DonthaveDevice({super.key});

  @override
  State<DonthaveDevice> createState() => _DonthaveDeviceState();
}

class _DonthaveDeviceState extends State<DonthaveDevice> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: background,
        elevation: 0,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(
            Icons.arrow_back_ios_new,
            color: unaicon,
          ),
        ),
        toolbarHeight: 100,
        leadingWidth: 30,
        centerTitle: false,
        title: Text(
          'National Diabetes Prevention Program',
          maxLines: 2,
          style: GoogleFonts.openSans(
            color: Color(0xFF303841),
            fontSize: 26,
            fontWeight: FontWeight.w700,
          ),
        ),
      ),
      body: Container(
        child: SingleChildScrollView(
          child: Column(
            children: [
              Padding(
                padding: EdgeInsets.only(
                  left: 50,
                  right: 50,
                  top: 30,
                ),
                child: Text(
                  'For the program, it is mandatory to connect a weighing scale. \n\nYou can order a new one here:',
                  style: GoogleFonts.openSans(
                    color: Color(0xFF0F1728),
                    fontSize: 18,
                    fontWeight: FontWeight.w400,
                  ),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Container(
                margin: EdgeInsets.symmetric(
                  horizontal: 50,
                ),
                width: double.infinity,
                height: 56,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(
                    8,
                  ),
                  color: aicon,
                ),
                child: ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all(
                      aicon,
                    ),
                    elevation: MaterialStateProperty.all(0),
                  ),
                  onPressed: () {},
                  child: Text(
                    'BROWSE SCALES',
                    style: GoogleFonts.openSans(
                      color: Color(0xFF303841),
                      fontSize: 20,
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Container(
                padding: EdgeInsets.symmetric(
                  horizontal: 50,
                ),
                child: GestureDetector(
                  onTap: () => Navigator.of(context).pop(),
                  child: Container(
                    width: double.infinity,
                    height: 30,
                    alignment: Alignment.center,
                    child: Text(
                      'I have a weighing scale',
                      textAlign: TextAlign.center,
                      style: GoogleFonts.openSans(
                        color: purple,
                        fontSize: 16,
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 30,
              ),
              Image.asset(
                "assets/images/ndppimage.png",
              ),
            ],
          ),
        ),
      ),
    );
  }
}
