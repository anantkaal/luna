import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:luna/condevicespages/dosagetimes.dart';
import 'package:luna/model.dart';
import 'package:luna/widgets/activebutton.dart';

class MedicationTracking extends StatefulWidget {
  const MedicationTracking({super.key});

  @override
  State<MedicationTracking> createState() => _MedicationTrackingState();
}

class _MedicationTrackingState extends State<MedicationTracking> {
  int nofdoses = 1;
  bool isML1 = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: background,
        elevation: 0,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(
            Icons.arrow_back_ios_new,
            color: unaicon,
          ),
        ),
        leadingWidth: 30,
        centerTitle: false,
        title: Text(
          "Medications",
          style: GoogleFonts.openSans(
            color: unaicon,
            fontSize: 26,
            fontWeight: FontWeight.w700,
          ),
        ),
      ),
      body: Container(
        padding: EdgeInsets.all(20),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Medication Name",
                style: GoogleFonts.openSans(
                  color: unaicon,
                  fontSize: 16,
                  fontWeight: FontWeight.w400,
                ),
              ),
              SizedBox(
                height: 5,
              ),
              Container(
                height: 48,
                width: double.infinity,
                padding: EdgeInsets.symmetric(horizontal: 10),
                decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border.all(
                    color: border,
                  ),
                  borderRadius: BorderRadius.circular(8),
                ),
                child: TextFormField(
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    hintText: "",
                  ),
                ),
              ),
              //S/D ends here
              SizedBox(
                height: 28,
              ),
              Text(
                "Quantity taken each time",
                style: GoogleFonts.openSans(
                  color: unaicon,
                  fontSize: 16,
                  fontWeight: FontWeight.w400,
                ),
              ),
              SizedBox(
                height: 5,
              ),
              Row(
                children: [
                  Container(
                    height: 48,
                    width: MediaQuery.of(context).size.width - 190,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      border: Border.all(
                        color: border,
                      ),
                      borderRadius: BorderRadius.circular(8),
                    ),
                    child: TextFormField(
                      keyboardType: TextInputType.number,
                      // validator: (value) {
                      //   if (value == null) {
                      //     return "this value shouldn't be empty";
                      //   }
                      //   final n = num.tryParse(value);
                      //   if (60 <= n!) {
                      //     return "should be less 60";
                      //   }
                      //   return null;
                      // },
                      textAlign: TextAlign.center,
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        hintText: "00",
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  //Toggle switch starts here
                  Container(
                    height: 48,
                    width: 140,
                    decoration: BoxDecoration(
                      color: box1,
                      borderRadius: BorderRadius.circular(8),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        GestureDetector(
                          onTap: () {
                            setState(() {
                              isML1 = true;
                            });
                          },
                          child: Container(
                            width: 50,
                            height: 28,
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                              color: isML1 ? purple : Colors.transparent,
                              borderRadius: BorderRadius.circular(8),
                            ),
                            child: Text(
                              "ML",
                              maxLines: 1,
                              textAlign: TextAlign.center,
                              style: GoogleFonts.openSans(
                                fontSize: 16,
                                fontWeight: FontWeight.w400,
                                color: isML1 ? Colors.white : unaicon,
                              ),
                            ),
                          ),
                        ),
                        GestureDetector(
                          onTap: () {
                            setState(() {
                              isML1 = false;
                            });
                          },
                          child: Container(
                            width: 50,
                            height: 28,
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                              color: isML1 ? Colors.transparent : purple,
                              borderRadius: BorderRadius.circular(8),
                            ),
                            child: Text(
                              textAlign: TextAlign.center,
                              maxLines: 1,
                              "Units",
                              style: GoogleFonts.openSans(
                                fontSize: 16,
                                fontWeight: FontWeight.w400,
                                color: isML1 ? unaicon : Colors.white,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  //Toggle switch ends here
                ],
              ),
              SizedBox(
                height: 28,
              ),
              Text(
                "Quantity taken each time",
                style: GoogleFonts.openSans(
                  color: unaicon,
                  fontSize: 16,
                  fontWeight: FontWeight.w400,
                ),
              ),
              SizedBox(
                height: 5,
              ),
              Container(
                height: 48,
                width: double.infinity,
                decoration: BoxDecoration(
                  border: Border.all(
                    color: border,
                  ),
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(8),
                ),
                child: DropdownButtonFormField(
                  padding: EdgeInsets.symmetric(
                    horizontal: 10,
                  ),
                  decoration: InputDecoration(
                    border: InputBorder.none,
                  ),
                  isExpanded: true,
                  icon: Icon(
                    Icons.keyboard_arrow_down_rounded,
                    color: purple,
                  ),
                  items: const [
                    DropdownMenuItem(
                      value: 0,
                      child: Text(
                        "0",
                      ),
                    ),
                    DropdownMenuItem(
                      value: 1,
                      child: Text(
                        "1",
                      ),
                    ),
                    DropdownMenuItem(
                      value: 2,
                      child: Text(
                        "2",
                      ),
                    ),
                    DropdownMenuItem(
                      value: 3,
                      child: Text(
                        "3",
                      ),
                    ),
                    DropdownMenuItem(
                      value: 4,
                      child: Text(
                        "4",
                      ),
                    ),
                  ],
                  onChanged: (value) {
                    setState(() {
                      nofdoses = value!;
                    });
                  },
                ),
              ),
              SizedBox(
                height: 28,
              ),
              Text(
                "Quantity taken each time",
                style: GoogleFonts.openSans(
                  color: unaicon,
                  fontSize: 16,
                  fontWeight: FontWeight.w400,
                ),
              ),
              SizedBox(
                height: 5,
              ),
              GestureDetector(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => DosageTimes(
                        nofdoses: nofdoses,
                      ),
                    ),
                  );
                },
                child: Container(
                  height: 48,
                  width: double.infinity,
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: border,
                    ),
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(8),
                  ),
                  padding: EdgeInsets.all(10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'Edit',
                        style: GoogleFonts.openSans(
                          color: Color(0xFF667084),
                          fontSize: 16,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                      Icon(
                        Icons.keyboard_arrow_right_rounded,
                      ),
                    ],
                  ),
                ),
              ),

              //Datepicker ends here
              SizedBox(
                height: 28,
              ),
              MainActiveButton(
                title: "NEXT",
                onTap: () {},
              ),
              SizedBox(
                height: 20,
              ),
              JustTextButton(
                text: 'Sync Other Devices',
                onpressed: () => Navigator.of(context).pop(),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
