import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:luna/model.dart';
import 'package:luna/pages.dart';

class WeightConnect extends StatefulWidget {
  const WeightConnect({super.key});

  @override
  State<WeightConnect> createState() => _WeightConnectState();
}

class _WeightConnectState extends State<WeightConnect> {
  @override
  Widget build(BuildContext context) {
    List _devices = [
      {
        'title': 'Fitbit Aria',
        'icon': 'assets/icons/devices/Fitbit.png',
      },
      {
        'title': 'Withings',
        'icon': 'assets/icons/devices/Withings.png',
      },
      {
        'title': 'Etekcity',
        'icon': 'assets/icons/devices/Etekcity.png',
      },
      {
        'title': 'Generic Bluetooth',
        'icon': 'assets/icons/devices/bluetooth.png',
      },
    ];

    return Scaffold(
      appBar: AppBar(
        backgroundColor: background,
        elevation: 0,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(
            Icons.arrow_back_ios_new,
            color: unaicon,
          ),
        ),
        leadingWidth: 30,
        centerTitle: false,
        title: Text(
          'Weight Tracking',
          style: GoogleFonts.openSans(
            color: Color(0xFF303841),
            fontSize: 26,
            fontWeight: FontWeight.w700,
          ),
        ),
      ),
      body: Container(
        padding: EdgeInsets.all(20),
        child: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                height: 380,
                child: GridView.builder(
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 3,
                    mainAxisSpacing: 15,
                    crossAxisSpacing: 15,
                    childAspectRatio: 2 / 2,
                  ),
                  shrinkWrap: true,
                  itemCount: _devices.length,
                  physics: NeverScrollableScrollPhysics(),
                  itemBuilder: (context, index) => GestureDetector(
                    onTap: () {
                      // Navigator.push(
                      //   context,
                      //   MaterialPageRoute(
                      //     builder: (context) => _devices[index]['navigation'],
                      //   ),
                      // );
                    },
                    child: Container(
                      height: 100,
                      width: 100,
                      decoration: BoxDecoration(
                        color: box,
                        borderRadius: BorderRadius.circular(
                          8,
                        ),
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          SizedBox(
                            height: 50,
                            child: Image.asset(
                              _devices[index]['icon'],
                            ),
                          ),
                          Text(
                            _devices[index]["title"],
                            textAlign: TextAlign.center,
                            style: GoogleFonts.openSans(
                              color: unaicon,
                              fontSize: 14,
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              Container(
                width: double.infinity,
                height: 56,
                padding: EdgeInsets.all(10),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(
                      8,
                    ),
                    color: aicon),
                child: ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all(
                      aicon,
                    ),
                    elevation: MaterialStateProperty.all(0),
                  ),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => SyncSuccess(),
                      ),
                    );
                  },
                  child: Text(
                    'SYNC',
                    style: GoogleFonts.openSans(
                      color: Color(0xFF303841),
                      fontSize: 20,
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              BorderedButton1(
                text: 'Don’t have a tracking device?',
                onpressed: () => Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => DonthaveDevice(),
                  ),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              JustTextButton(
                text: 'Sync Other Devices',
                onpressed: () => Navigator.of(context).pop(),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
