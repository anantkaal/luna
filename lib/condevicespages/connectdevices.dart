import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:luna/condevicespages/cdmodel.dart';
import 'package:luna/pages.dart';
import 'package:luna/model.dart';

class ConnectDevices extends StatefulWidget {
  const ConnectDevices({super.key});

  @override
  State<ConnectDevices> createState() => _ConnectDevicesState();
}

class _ConnectDevicesState extends State<ConnectDevices> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: background,
        elevation: 0,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(
            Icons.arrow_back_ios_new,
            color: unaicon,
          ),
        ),
        leadingWidth: 30,
        centerTitle: false,
        title: Text(
          'Sync Devices & Apps',
          style: GoogleFonts.openSans(
            color: Color(0xFF303841),
            fontSize: 26,
            fontWeight: FontWeight.w700,
          ),
        ),
      ),
      body: SafeArea(
        child: Container(
          padding: EdgeInsets.all(20),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  height: 390,
                  margin: EdgeInsets.only(
                      // bottom: 30,
                      ),
                  child: GridView.builder(
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 3,
                      mainAxisSpacing: 15,
                      crossAxisSpacing: 15,
                      childAspectRatio: 2 / 2,
                    ),
                    shrinkWrap: true,
                    itemCount: connectpages.length,
                    physics: NeverScrollableScrollPhysics(),
                    itemBuilder: (context, index) => GestureDetector(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) =>
                                connectpages[index]['navigation'],
                          ),
                        );
                      },
                      child: Container(
                        height: 100,
                        width: 100,
                        decoration: BoxDecoration(
                          color: box,
                          borderRadius: BorderRadius.circular(
                            8,
                          ),
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            SizedBox(
                              height: 50,
                              child: SvgPicture.asset(
                                connectpages[index]['icon'],
                                fit: BoxFit.fitWidth,
                              ),
                            ),
                            Text(
                              connectpages[index]["title"],
                              textAlign: TextAlign.center,
                              style: GoogleFonts.openSans(
                                color: unaicon,
                                fontSize: 14,
                                fontWeight: FontWeight.w400,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                MainActiveButton(
                  title: 'TRACK!',
                  onTap: () {},
                ),
                SizedBox(
                  height: 20,
                ),
                JustTextButton(
                  text: 'Sync Other Devices',
                  onpressed: () => Navigator.of(context).pop(),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
