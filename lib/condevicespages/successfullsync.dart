import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:luna/pages.dart';

import '../model.dart';

class SyncSuccess extends StatefulWidget {
  const SyncSuccess({super.key});

  @override
  State<SyncSuccess> createState() => _SyncSuccessState();
}

class _SyncSuccessState extends State<SyncSuccess> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          padding: EdgeInsets.all(25),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Great!",
                style: GoogleFonts.openSans(
                  color: Color(0xFF303841),
                  fontSize: 26,
                  fontWeight: FontWeight.w700,
                ),
              ),
              Text(
                "You’re fully Synced",
                style: GoogleFonts.openSans(
                  color: Color(0xFF303841),
                  fontSize: 26,
                  fontWeight: FontWeight.w700,
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Text(
                'You can now view your stats, daily plan and track your fitness journey on Jaya',
                style: GoogleFonts.openSans(
                  color: Color(0xFF0F1728),
                  fontSize: 18,
                  fontWeight: FontWeight.w400,
                ),
              ),
              SizedBox(
                height: 40,
              ),
              Container(
                width: double.infinity,
                height: 56,
                padding: EdgeInsets.all(10),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(
                      8,
                    ),
                    color: aicon),
                child: ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all(
                      aicon,
                    ),
                    elevation: MaterialStateProperty.all(0),
                  ),
                  onPressed: () {
                    Navigator.pop(context);
                    Navigator.pop(context);
                  },
                  child: Text(
                    'Let’s Go',
                    style: GoogleFonts.openSans(
                      color: Color(0xFF303841),
                      fontSize: 20,
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 40,
              ),
              Image.asset(
                "assets/images/successimage.png",
              ),
            ],
          ),
        ),
      ),
    );
  }
}
