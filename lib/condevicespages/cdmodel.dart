import 'package:luna/condevicespages/activityconnect.dart';
import 'package:luna/condevicespages/bpconnect.dart';
import 'package:luna/condevicespages/bsugarconnect.dart';
import 'package:luna/condevicespages/foodconnect.dart';
import 'package:luna/condevicespages/medicationtrack.dart';
import 'package:luna/condevicespages/mindfuldevices.dart';
import 'package:luna/condevicespages/sleepConnect.dart';
import 'package:luna/pages.dart';

List connectpages = [
  {
    'icon': "assets/icons/weight.svg",
    'title': "Weight",
    'navigation': WeightConnect(),
  },
  {
    'icon': "assets/icons/activity.svg",
    'title': "Activity",
    'navigation': ActivityConnect(),
  },
  {
    'icon': "assets/icons/medication.svg",
    'title': "Medication",
    'navigation': MedicationTracking(),
  },
  {
    'icon': "assets/icons/Food.svg",
    'title': "Food",
    'navigation': FoodConnect()
  },
  {
    'icon': "assets/icons/sleep.svg",
    'title': "Sleep",
    'navigation': SleepConnect(),
  },
  {
    'icon': "assets/icons/bloodsugar.svg",
    'title': 'Blood Sugar',
    'navigation': BSConnect(),
  },
  {
    'icon': "assets/icons/bloodpressure.svg",
    'title': "Blood   Pressure",
    'navigation': BPConnect(),
  },
  {
    'icon': "assets/icons/mindfullness.svg",
    'title': "Mindfullness",
    'navigation': MindDevices()
  },
  {
    'icon': "assets/icons/Cholesterol.svg",
    'title': "Cholesterol",
    'navigation': Cholesterol(
      name: "Cholesterol",
    ),
  },
];
