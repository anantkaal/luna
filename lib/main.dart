import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:luna/Food/addnewdish.dart';
import 'package:luna/Food/editmeal.dart';
import 'package:luna/Food/foodlogs.dart';
import 'package:luna/condevicespages/connectdevices.dart';
import 'package:luna/model.dart';

void main() {
  runApp(const Homepage());
}

class Homepage extends StatelessWidget {
  const Homepage({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primaryColor: purple,
      ),
      debugShowCheckedModeBanner: false,
      home: MainApp(),
      routes: {
        '/connectdevice': (context) => ConnectDevices(),
      },
    );
  }
}

class MainApp extends StatefulWidget {
  const MainApp({super.key});

  @override
  State<MainApp> createState() => _MainAppState();
}

class _MainAppState extends State<MainApp> {
  int activeTab = 2;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      drawer: SafeArea(
        child: Align(
          alignment: Alignment.topLeft,
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              color: Colors.white,
            ),
            padding: EdgeInsets.all(10),
            alignment: Alignment.topLeft,
            width: MediaQuery.of(context).size.width * 0.65,
            height: MediaQuery.of(context).size.height * 0.65,
            child: Drawer(
              backgroundColor: Colors.white,
              elevation: 0,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: 30,
                    child: IconButton(
                      onPressed: () => _scaffoldKey.currentState!.closeDrawer(),
                      icon: Icon(
                        Icons.close,
                        size: 26,
                      ),
                    ),
                  ),
                  ListTile(
                    leading: CircleAvatar(
                      backgroundImage: AssetImage(
                        "assets/images/dp.png",
                      ),
                    ),
                    title: Text(
                      'Olivia Rhye',
                      style: GoogleFonts.inter(
                        color: Color(0xFF344053),
                        fontSize: 16,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    subtitle: Text(
                      'olivia@untitledui.com',
                      maxLines: 1,
                      style: TextStyle(
                        color: Color(0xFF667084),
                        fontSize: 14,
                        fontFamily: 'Inter',
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: SizedBox(
                          height: 20,
                          width: 20,
                          child: SvgPicture.asset(
                            "assets/icons/settings.svg",
                            fit: BoxFit.fitHeight,
                          ),
                        ),
                      ),
                      Text(
                        'Settings',
                        style: GoogleFonts.inter(
                          color: Color(0xFF344053),
                          fontSize: 16,
                          fontWeight: FontWeight.w500,
                        ),
                      )
                    ],
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: SizedBox(
                          height: 20,
                          width: 20,
                          child: SvgPicture.asset(
                            "assets/icons/dcalender.svg",
                            fit: BoxFit.fitHeight,
                          ),
                        ),
                      ),
                      Text(
                        'Today’s plan',
                        style: GoogleFonts.inter(
                          color: Color(0xFF344053),
                          fontSize: 16,
                          fontWeight: FontWeight.w500,
                        ),
                      )
                    ],
                  ),
                  Divider(
                    thickness: 1,
                  ),
                  GestureDetector(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => ConnectDevices(),
                        ),
                      );
                    },
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: SizedBox(
                            height: 20,
                            width: 20,
                            child: SvgPicture.asset(
                              "assets/icons/homeweight.svg",
                              fit: BoxFit.fitHeight,
                            ),
                          ),
                        ),
                        Text(
                          'Connect Devices',
                          style: GoogleFonts.inter(
                            color: Color(0xFF344053),
                            fontSize: 16,
                            fontWeight: FontWeight.w500,
                          ),
                        )
                      ],
                    ),
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: SizedBox(
                          height: 20,
                          width: 20,
                          child: SvgPicture.asset(
                            "assets/icons/nutrition.svg",
                            fit: BoxFit.fitHeight,
                          ),
                        ),
                      ),
                      Text(
                        'My Nutrition',
                        style: GoogleFonts.inter(
                          color: Color(0xFF344053),
                          fontSize: 16,
                          fontWeight: FontWeight.w500,
                        ),
                      )
                    ],
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: SizedBox(
                          height: 20,
                          width: 20,
                          child: SvgPicture.asset(
                            "assets/icons/healthmodel.svg",
                            fit: BoxFit.fitHeight,
                          ),
                        ),
                      ),
                      Text(
                        'Health Model',
                        style: GoogleFonts.inter(
                          color: Color(0xFF344053),
                          fontSize: 16,
                          fontWeight: FontWeight.w500,
                        ),
                      )
                    ],
                  ),
                  Divider(
                    thickness: 1,
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: SizedBox(
                          height: 20,
                          width: 20,
                          child: SvgPicture.asset(
                            "assets/icons/dndpp.svg",
                            fit: BoxFit.fitHeight,
                          ),
                        ),
                      ),
                      Flexible(
                        child: Text(
                          'National Diabetes Prevention Program',
                          style: GoogleFonts.inter(
                            color: Color(0xFF344053),
                            fontSize: 16,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      )
                    ],
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: SizedBox(
                          height: 20,
                          width: 20,
                          child: SvgPicture.asset(
                            "assets/icons/dresources.svg",
                            fit: BoxFit.fitHeight,
                            color: unaicon,
                          ),
                        ),
                      ),
                      Text(
                        'Resources',
                        style: GoogleFonts.inter(
                          color: Color(0xFF344053),
                          fontSize: 16,
                          fontWeight: FontWeight.w500,
                        ),
                      )
                    ],
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: SizedBox(
                          height: 20,
                          width: 20,
                          child: SvgPicture.asset(
                            "assets/icons/help.svg",
                            fit: BoxFit.fitHeight,
                          ),
                        ),
                      ),
                      Text(
                        'Support',
                        style: GoogleFonts.inter(
                          color: Color(0xFF344053),
                          fontSize: 16,
                          fontWeight: FontWeight.w500,
                        ),
                      )
                    ],
                  ),
                  Divider(
                    thickness: 1,
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: SizedBox(
                          height: 20,
                          width: 20,
                          child: SvgPicture.asset(
                            "assets/icons/logout.svg",
                            fit: BoxFit.fitHeight,
                          ),
                        ),
                      ),
                      Text(
                        'Log out',
                        style: GoogleFonts.inter(
                          color: Color(0xFF344053),
                          fontSize: 16,
                          fontWeight: FontWeight.w500,
                        ),
                      )
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
      appBar: AppBar(
        centerTitle: false,
        backgroundColor: background,
        elevation: 0,
        leadingWidth: 40,
        iconTheme: IconThemeData(
          color: unaicon,
        ),
        // leading: IconButton(
        //   onPressed: () {},
        //   icon: SvgPicture.asset(
        //     "assets/icons/burgermenu.svg",
        //   ),
        // ),
        leading: Builder(
          builder: (context) => IconButton(
            icon: SvgPicture.asset(
              "assets/icons/burgermenu.svg",
            ),
            onPressed: () => _scaffoldKey.currentState!.openDrawer(),
          ),
        ),
        title: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Hello",
              style: GoogleFonts.openSans(
                color: unaicon,
                fontSize: 14,
                fontWeight: FontWeight.w400,
              ),
            ),
            Text(
              "Username!",
              style: GoogleFonts.openSans(
                color: unaicon,
                fontSize: 18,
                fontWeight: FontWeight.w600,
              ),
            ),
          ],
        ),
      ),
      body: navpages[activeTab],
      bottomNavigationBar: getFooter(),
    );
  }

  Widget getFooter() {
    final size = MediaQuery.of(context).size;
    return Material(
      elevation: 10,
      shadowColor: Colors.black,
      child: Container(
        alignment: Alignment.center,
        decoration: BoxDecoration(
            // color: Colors.amber,
            ),
        height: size.height * 0.1,
        child: Padding(
          padding: EdgeInsets.only(
            left: 5,
            right: 5,
          ),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: List.generate(navicons.length, (index) {
              return Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  // SizedBox(
                  //   height: size.height * 0.015,
                  // ),
                  GestureDetector(
                    onTap: () {
                      setState(() {
                        activeTab = index;
                      });
                    },
                    child: Container(
                      height: 25,
                      width: size.width * 0.15,
                      child: SvgPicture.asset(
                        navicons[index]['icon'],
                        color: activeTab == index ? aicon : unaicon,
                        fit: BoxFit.fitHeight,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Text(
                    navicons[index]['title'],
                    style: GoogleFonts.openSans(
                      fontSize: 12,
                      color: unaicon,
                    ),
                  ),
                ],
              );
            }),
          ),
        ),
      ),
    );
  }
}
