import 'package:flutter/material.dart';
import 'package:luna/pages.dart';

final unaicon = Color.fromRGBO(48, 56, 65, 1);
final aicon = Color.fromRGBO(255, 225, 93, 1);
final background = Color.fromRGBO(252, 252, 252, 1);
final box = Color.fromRGBO(241, 241, 241, 1);
final boxinner = Color.fromRGBO(245, 245, 245, 1);
final border = Color.fromRGBO(105, 105, 105, 0.25);
final box1 = Color.fromRGBO(240, 241, 242, 1);
final purple = Color.fromRGBO(176, 136, 249, 1);
final red = Color.fromRGBO(180, 61, 53, 1);
final grey = Color.fromRGBO(102, 112, 133, 1);
final pie1 = Color.fromRGBO(139, 211, 221, 1);
final pie2 = Color.fromRGBO(255, 219, 204, 1);
final pie3 = Color.fromRGBO(251, 212, 254, 1);
final pie4 = Color.fromRGBO(255, 225, 93, 0.6);
final pie5 = Color.fromRGBO(84, 180, 53, 0.6);
final green = Color.fromRGBO(205, 255, 204, 1);
final pink = Color.fromRGBO(251, 212, 254, 1);
final blue = Color.fromRGBO(162, 219, 227, 0.8);
final orange = Color.fromRGBO(255, 219, 204, 1);
final border1 = Color.fromRGBO(232, 232, 232, 1);

final grey1 = Color.fromRGBO(242, 244, 247, 1);

int metricpage = 0;

void showsnackbar(context, color, message) {
  ScaffoldMessenger.of(context).showSnackBar(
    SnackBar(
      content: Text(
        message,
        style: const TextStyle(fontSize: 14),
      ),
      backgroundColor: color,
      duration: const Duration(seconds: 2),
      action: SnackBarAction(
        label: "OK",
        onPressed: () {},
        textColor: Colors.white,
      ),
    ),
  );
}

List fooditems = [
  "Egg whites 3",
  "Baked potatoes 100g",
  "Cooked rice 1 cup",
  "Mung Dal 100g",
  "Peanuts 100g",
  "Milk fat free 1 cup",
  "Greek Yogurt 100g",
];

List resources = [
  {
    "title": "Meal Plan",
    "icon": "assets/icons/meal.svg",
  },
  {
    "title": "Find Food",
    "icon": "assets/icons/findfood.svg",
  },
  {
    "title": "Grocery Stores",
    "icon": "assets/icons/grocery.svg",
  },
  {
    "title": "Fitness Centers",
    "icon": "assets/icons/fitnesscentre.svg",
  },
  {
    "title": "Pharmacies",
    "icon": "assets/icons/pharmacy.svg",
  },
  {
    "title": "Hospitals",
    "icon": "assets/icons/hospitals.svg",
  },
  {
    "title": "Help",
    "icon": "assets/icons/help.svg",
  },
  // "Egg whites 3",
  // "Baked potatoes 100g",
  // "Cooked rice 1 cup",
  // "Mung Dal 100g",
  // "Peanuts 100g",
  // "Milk fat free 1 cup",
  // "Greek Yogurt 100g",
];

List metrices = [
  {
    'icon': "assets/icons/bloodsugar.svg",
    'title': 'Blood Sugar',
    'navigation': BloodSugar(
      name: "Blood Sugar",
    ),
  },
  {
    'icon': "assets/icons/bloodpressure.svg",
    'title': "Blood   Pressure",
    'navigation': BloodPressure(
      name: "Blood Pressure",
    ),
  },
  {
    'icon': "assets/icons/medication.svg",
    'title': "Medication",
    'navigation': Medication(
      name: "Medication",
    ),
  },
  {
    'icon': "assets/icons/aic.svg",
    'title': "A1C",
    'navigation': A1C(
      name: "A1C",
    ),
  },
  {
    'icon': "assets/icons/Cholesterol.svg",
    'title': "Cholesterol",
    'navigation': Cholesterol(
      name: "Cholesterol",
    ),
  },
  {
    'icon': "assets/icons/weight.svg",
    'title': "Weight",
    'navigation': Weight(
      name: "Weight",
    ),
  },
  {
    'icon': "assets/icons/activity.svg",
    'title': "Activity",
    'navigation': Activity(
      name: "Activity Tracking",
    ),
  },
  {
    'icon': "assets/icons/Food.svg",
    'title': "Food",
    'navigation': Food(
      name: "Food Tracking",
    ),
  },
  {
    'icon': "assets/icons/sleep.svg",
    'title': "Sleep",
    'navigation': Sleep(
      name: "Sleep",
    ),
  },
  {
    'icon': "assets/icons/mindfullness.svg",
    'title': "Mindfullness",
    'navigation': MindFullNess(
      name: "MindFullNess",
    ),
  },
  {
    'icon': "assets/icons/measurements.svg",
    'title': "Measurements",
    'navigation': Measurements(
      name: "Measurements",
    ),
  },
  {
    'icon': "assets/icons/composition.svg",
    'title': "Composition",
    'navigation': Composition(
      name: "Composition",
    ),
  },
];

List navpages = [
  Home(),
  NDPP(),
  Plan(),
  Journey(),
  Help(),
];

List navicons = [
  {
    'icon': "assets/icons/home.svg",
    'title': 'Home',
  },
  {
    'icon': "assets/icons/ndpp.svg",
    'title': "N.DPP",
  },
  {
    'icon': "assets/icons/plan.svg",
    'title': "Plan",
  },
  {
    'icon': "assets/icons/journey.svg",
    'title': "Journey",
  },
  {
    'icon': "assets/icons/help.svg",
    'title': "Help",
  },
];






// body: Container(
//         padding: EdgeInsets.all(20),
//         child: SingleChildScrollView(
//           child: Column(
//             crossAxisAlignment: CrossAxisAlignment.start,
//             children: [
//               Text(
//                 "Manual Tracking",
//                 style: TextStyle(
//                   color: unaicon,
//                   fontSize: 25,
//                   fontWeight: FontWeight.w700,
//                 ),
//               ),
//               SizedBox(
//                 height: 30,
//               ),
//               GridView.builder(
//                 gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
//                   crossAxisCount: 3,
//                   mainAxisSpacing: 15,
//                   crossAxisSpacing: 15,
//                   childAspectRatio: 2 / 2,
//                 ),
//                 // physics: ScrollPhysics(),
//                 shrinkWrap: true,
//                 itemCount: metrices.length - 1,

//                 itemBuilder: (context, index) => GestureDetector(
//                   onTap: () {
//                     setState(() {
//                       metricpage = index;
//                       print(metricpage);
//                     });
//                   },
//                   child: Container(
//                     height: 100,
//                     width: 100,
//                     decoration: BoxDecoration(
//                       color: box,
//                       borderRadius: BorderRadius.circular(
//                         8,
//                       ),
//                     ),
//                     child: Column(
//                       crossAxisAlignment: CrossAxisAlignment.center,
//                       mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                       children: [
//                         SvgPicture.asset(
//                           metrices[index ]['icon'],
//                         ),
//                         Text(
//                           metrices[index ]["title"],
//                           textAlign: TextAlign.center,
//                           style: TextStyle(
//                             color: unaicon,
//                             fontSize: 14,
//                             fontWeight: FontWeight.w400,
//                           ),
//                         ),
//                       ],
//                     ),
//                   ),
//                 ),
//               ),
//               SizedBox(
//                 height: 30,
//               ),
//               Container(
//                 alignment: Alignment.center,
//                 decoration: BoxDecoration(
//                   borderRadius: BorderRadius.circular(
//                     8,
//                   ),
//                   color: aicon,
//                 ),
//                 width: double.infinity,
//                 height: 56,
//                 child: Text(
//                   "Update",
//                   style: TextStyle(
//                     color: unaicon,
//                     fontSize: 20,
//                     fontWeight: FontWeight.w700,
//                   ),
//                 ),
//               ),
//             ],
//           ),
//         ),
//       ),



// bottomNavigationBar: BottomNavigationBar(
        //   items: [
        //     BottomNavigationBarItem(
        //       icon: SvgPicture.asset(
        //         navicons[0]['icon'],
        //       ),
        //       label: navicons[0]['title'],
        //     ),
        //     BottomNavigationBarItem(
        //       icon: SvgPicture.asset(
        //         navicons[0]['icon'],
        //       ),
        //       label: navicons[0]['title'],
        //     ),
        //     BottomNavigationBarItem(
        //       icon: SvgPicture.asset(
        //         navicons[0]['icon'],
        //       ),
        //       label: navicons[0]['title'],
        //     ),
        //     BottomNavigationBarItem(
        //       icon: SvgPicture.asset(
        //         navicons[0]['icon'],
        //       ),
        //       label: navicons[0]['title'],
        //     ),
        //   ],
        // ),