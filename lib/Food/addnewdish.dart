import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:luna/model.dart';

class AddNewDish extends StatefulWidget {
  const AddNewDish({super.key});

  @override
  State<AddNewDish> createState() => _AddNewDishState();
}

class _AddNewDishState extends State<AddNewDish> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: background,
        elevation: 0,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(
            Icons.arrow_back_ios_new,
            color: unaicon,
          ),
        ),
        leadingWidth: 30,
        centerTitle: false,
        title: Text(
          "Add New Dish",
          style: GoogleFonts.openSans(
            color: unaicon,
            fontSize: 24,
            fontWeight: FontWeight.w700,
          ),
        ),
      ),
      body: Container(
        padding: EdgeInsets.all(20),
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Name of the dish",
                      style: GoogleFonts.openSans(
                        color: unaicon,
                        fontSize: 16,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 10),
                      height: 48,
                      width: double.infinity,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        border: Border.all(
                          color: border,
                        ),
                        borderRadius: BorderRadius.circular(8),
                      ),
                      child: TextFormField(
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.symmetric(horizontal: 20),
                          border: InputBorder.none,
                          hintText: "Chicken Shawarma DIY",
                        ),
                      ),
                    )
                  ],
                ),
              ),
              SizedBox(
                height: 28,
              ),
              Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Brand / Restaurant",
                      style: GoogleFonts.openSans(
                        color: unaicon,
                        fontSize: 16,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 10),
                      height: 48,
                      width: double.infinity,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        border: Border.all(
                          color: border,
                        ),
                        borderRadius: BorderRadius.circular(8),
                      ),
                      child: TextFormField(
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.symmetric(horizontal: 20),
                          border: InputBorder.none,
                          hintText: "Subway",
                        ),
                      ),
                    )
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 0, vertical: 0),
                child: GridView(
                  clipBehavior: Clip.hardEdge,
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                    childAspectRatio: 4 / 2,
                    crossAxisSpacing: 10,
                  ),
                  children: [
                    textField(
                      "Calories (Per serving)",
                    ),
                    textField(
                      "Calories (Per 100g)",
                    ),
                    textField(
                      "Protein (Per serving)",
                    ),
                    textField(
                      "Protein (Per 100g)",
                    ),
                    textField(
                      "Calories (Per serving)",
                    ),
                    textField(
                      "Calories (Per 100g)",
                    ),
                    textField(
                      "Fat (Per serving)",
                    ),
                    textField(
                      "Fat (Per 100g)",
                    ),
                    textField(
                      "Net carbs (Per serving)",
                    ),
                    textField(
                      "Net carbs (Per 100g)",
                    ),
                    textField(
                      "Fiber (Per serving)",
                    ),
                    textField(
                      "Fiber (Per 100g)",
                    ),
                    textField(
                      "Total carbs (Per serving)",
                    ),
                    textField(
                      "Total carbs (Per 100g)",
                    ),
                  ],
                ),
              ),
              Container(
                width: double.infinity,
                height: 56,
                child: ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all(
                      aicon,
                    ),
                  ),
                  onPressed: () {
                    // if (_formKey.currentState!.validate()) {

                    //   print('Form is valid');
                    // }
                  },
                  child: Text(
                    "Save",
                    style: GoogleFonts.openSans(
                      color: unaicon,
                      fontSize: 20,
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 28,
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget textField(
    String label,
  ) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Text(
            label,
            style: GoogleFonts.openSans(
              color: unaicon,
              fontSize: 14,
              fontWeight: FontWeight.w400,
            ),
          ),
          Container(
            height: 48,
            width: double.infinity,
            decoration: BoxDecoration(
              color: Colors.white,
              border: Border.all(
                color: border,
              ),
              borderRadius: BorderRadius.circular(8),
            ),
            child: TextFormField(
              keyboardType: TextInputType.number,
              textAlign: TextAlign.center,
              decoration: InputDecoration(
                border: InputBorder.none,
                hintText: "00",
              ),
            ),
          ),
        ],
      ),
    );
  }
}
