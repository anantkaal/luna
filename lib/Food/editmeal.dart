import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:luna/model.dart';

class EditMealScreen extends StatefulWidget {
  const EditMealScreen({super.key});

  @override
  State<EditMealScreen> createState() => _EditMealScreenState();
}

class _EditMealScreenState extends State<EditMealScreen> {
  @override
  Widget build(BuildContext context) {
    bool isKM = true;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: background,
        elevation: 0,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(
            Icons.arrow_back_ios_new,
            color: unaicon,
          ),
        ),
        leadingWidth: 30,
        centerTitle: false,
        title: Text(
          "Edit Meal",
          style: GoogleFonts.openSans(
            color: unaicon,
            fontSize: 24,
            fontWeight: FontWeight.w700,
          ),
        ),
      ),
      body: Container(
        padding: EdgeInsets.all(20),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Name of the dish",
                      style: GoogleFonts.openSans(
                        color: unaicon,
                        fontSize: 16,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 10),
                      height: 48,
                      width: double.infinity,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        border: Border.all(
                          color: border,
                        ),
                        borderRadius: BorderRadius.circular(8),
                      ),
                      child: TextFormField(
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.symmetric(horizontal: 20),
                          border: InputBorder.none,
                          hintText: "Chicken Shawarma DIY",
                        ),
                      ),
                    )
                  ],
                ),
              ),
              SizedBox(
                height: 28,
              ),
              Text(
                "Distance",
                style: GoogleFonts.openSans(
                  color: unaicon,
                  fontSize: 16,
                  fontWeight: FontWeight.w400,
                ),
              ),
              Row(
                children: [
                  Container(
                    height: 48,
                    width: 142,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      border: Border.all(
                        color: border,
                      ),
                      borderRadius: BorderRadius.circular(8),
                    ),
                    child: TextFormField(
                      keyboardType: TextInputType.number,
                      // validator: (value) {
                      //   if (value == null) {
                      //     return "this value shouldn't be empty";
                      //   }
                      //   final n = num.tryParse(value);
                      //   if (60 <= n!) {
                      //     return "should be less 60";
                      //   }
                      //   return null;
                      // },
                      textAlign: TextAlign.center,
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        hintText: "100",
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  //Toggle switch starts here
                  Container(
                    height: 48,
                    width: 120,
                    decoration: BoxDecoration(
                      color: box1,
                      borderRadius: BorderRadius.circular(8),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        GestureDetector(
                          onTap: () {
                            setState(() {
                              isKM = true;
                            });
                          },
                          child: Container(
                            width: 50,
                            height: 28,
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                              color: isKM ? purple : Colors.transparent,
                              borderRadius: BorderRadius.circular(8),
                            ),
                            child: Text(
                              "KM",
                              maxLines: 1,
                              textAlign: TextAlign.center,
                              style: GoogleFonts.openSans(
                                fontSize: 16,
                                fontWeight: FontWeight.w400,
                                color: isKM ? Colors.white : unaicon,
                              ),
                            ),
                          ),
                        ),
                        GestureDetector(
                          onTap: () {
                            setState(() {
                              isKM = false;
                            });
                          },
                          child: Container(
                            width: 50,
                            height: 28,
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                              color: isKM ? Colors.transparent : purple,
                              borderRadius: BorderRadius.circular(8),
                            ),
                            child: Text(
                              textAlign: TextAlign.center,
                              maxLines: 1,
                              "Miles",
                              style: GoogleFonts.openSans(
                                fontSize: 14,
                                fontWeight: FontWeight.w400,
                                color: isKM ? unaicon : Colors.white,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  //Toggle switch ends here
                ],
              ),
              SizedBox(
                height: 28,
              ),
              Text(
                "Calories",
                style: GoogleFonts.openSans(
                  color: unaicon,
                  fontSize: 16,
                  fontWeight: FontWeight.w400,
                ),
              ),
              Row(
                children: [
                  Container(
                    height: 48,
                    width: 142,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      border: Border.all(
                        color: border,
                      ),
                      borderRadius: BorderRadius.circular(8),
                    ),
                    child: TextFormField(
                      keyboardType: TextInputType.number,
                      textAlign: TextAlign.center,
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        hintText: "00",
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Container(
                    height: 48,
                    width: 120,
                    decoration: BoxDecoration(
                      color: box1,
                      borderRadius: BorderRadius.circular(8),
                    ),
                    alignment: Alignment.center,
                    child: Text(
                      "Kcal",
                      style: GoogleFonts.openSans(
                        fontSize: 16,
                        fontWeight: FontWeight.w400,
                        color: unaicon,
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 28,
              ),
              Text(
                "Protein",
                style: GoogleFonts.openSans(
                  color: unaicon,
                  fontSize: 16,
                  fontWeight: FontWeight.w400,
                ),
              ),
              Row(
                children: [
                  Container(
                    height: 48,
                    width: 142,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      border: Border.all(
                        color: border,
                      ),
                      borderRadius: BorderRadius.circular(8),
                    ),
                    child: TextFormField(
                      keyboardType: TextInputType.number,
                      textAlign: TextAlign.center,
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        hintText: "00",
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Container(
                    height: 48,
                    width: 120,
                    decoration: BoxDecoration(
                      color: box1,
                      borderRadius: BorderRadius.circular(8),
                    ),
                    alignment: Alignment.center,
                    child: Text(
                      "Gms",
                      style: GoogleFonts.openSans(
                        fontSize: 16,
                        fontWeight: FontWeight.w400,
                        color: unaicon,
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 28,
              ),
              Text(
                "Fats",
                style: GoogleFonts.openSans(
                  color: unaicon,
                  fontSize: 16,
                  fontWeight: FontWeight.w400,
                ),
              ),
              Row(
                children: [
                  Container(
                    height: 48,
                    width: 142,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      border: Border.all(
                        color: border,
                      ),
                      borderRadius: BorderRadius.circular(8),
                    ),
                    child: TextFormField(
                      keyboardType: TextInputType.number,
                      textAlign: TextAlign.center,
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        hintText: "00",
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Container(
                    height: 48,
                    width: 120,
                    decoration: BoxDecoration(
                      color: box1,
                      borderRadius: BorderRadius.circular(8),
                    ),
                    alignment: Alignment.center,
                    child: Text(
                      "Gms",
                      style: GoogleFonts.openSans(
                        fontSize: 16,
                        fontWeight: FontWeight.w400,
                        color: unaicon,
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 28,
              ),
              Text(
                "Net Carbs",
                style: GoogleFonts.openSans(
                  color: unaicon,
                  fontSize: 16,
                  fontWeight: FontWeight.w400,
                ),
              ),
              Row(
                children: [
                  Container(
                    height: 48,
                    width: 142,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      border: Border.all(
                        color: border,
                      ),
                      borderRadius: BorderRadius.circular(8),
                    ),
                    child: TextFormField(
                      keyboardType: TextInputType.number,
                      textAlign: TextAlign.center,
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        hintText: "00",
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Container(
                    height: 48,
                    width: 120,
                    decoration: BoxDecoration(
                      color: box1,
                      borderRadius: BorderRadius.circular(8),
                    ),
                    alignment: Alignment.center,
                    child: Text(
                      "Gms",
                      style: GoogleFonts.openSans(
                        fontSize: 16,
                        fontWeight: FontWeight.w400,
                        color: unaicon,
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 28,
              ),
              Text(
                "Fibers",
                style: GoogleFonts.openSans(
                  color: unaicon,
                  fontSize: 16,
                  fontWeight: FontWeight.w400,
                ),
              ),
              Row(
                children: [
                  Container(
                    height: 48,
                    width: 142,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      border: Border.all(
                        color: border,
                      ),
                      borderRadius: BorderRadius.circular(8),
                    ),
                    child: TextFormField(
                      keyboardType: TextInputType.number,
                      textAlign: TextAlign.center,
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        hintText: "00",
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Container(
                    height: 48,
                    width: 120,
                    decoration: BoxDecoration(
                      color: box1,
                      borderRadius: BorderRadius.circular(8),
                    ),
                    alignment: Alignment.center,
                    child: Text(
                      "Gms",
                      style: GoogleFonts.openSans(
                        fontSize: 16,
                        fontWeight: FontWeight.w400,
                        color: unaicon,
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 28,
              ),
              Container(
                width: double.infinity,
                height: 56,
                child: ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all(
                      aicon,
                    ),
                  ),
                  onPressed: () {
                    // if (_formKey.currentState!.validate()) {

                    //   print('Form is valid');
                    // }
                  },
                  child: Text(
                    "Save",
                    style: GoogleFonts.openSans(
                      color: unaicon,
                      fontSize: 20,
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 40,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
