import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:luna/model.dart';
import 'package:luna/pages.dart';

class FoodLogsPage extends StatefulWidget {
  const FoodLogsPage({super.key});

  @override
  State<FoodLogsPage> createState() => _FoodLogsPageState();
}

class _FoodLogsPageState extends State<FoodLogsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: background,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(
            Icons.arrow_back_ios_new,
            color: unaicon,
          ),
        ),
        elevation: 0,
        title: Text(
          'Food Log',
          style: GoogleFonts.openSans(
            color: unaicon,
            fontSize: 24,
            fontWeight: FontWeight.w700,
          ),
        ),
        centerTitle: false,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.all(10),
              height: 56,
              width: double.infinity,
              alignment: Alignment.centerLeft,
              decoration: BoxDecoration(
                  border: Border.all(
                    color: border,
                  ),
                  borderRadius: BorderRadius.circular(8)),
              child: TextFormField(
                textAlign: TextAlign.start,
                decoration: InputDecoration(
                  border: InputBorder.none,
                  hintText: "  Search for Food",
                  hintStyle: GoogleFonts.openSans(
                    color: grey,
                    fontSize: 16,
                    fontWeight: FontWeight.w400,
                  ),
                  prefixIcon: Icon(
                    Icons.search,
                    size: 24,
                    color: unaicon,
                  ),
                  // prefixIcon: SizedBox(
                  //   height: 19,
                  //   width: 19,
                  //   child: SvgPicture.asset(
                  //     "assets/icons/search.svg",
                  //     fit: BoxFit.fill,
                  //   ),
                  // ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Recently searched foods',
                    style: GoogleFonts.openSans(
                      color: unaicon,
                      fontSize: 16,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => AddNewDish(),
                        ),
                      );
                    },
                    child: Container(
                      padding: EdgeInsets.symmetric(
                        horizontal: 13,
                        vertical: 4,
                      ),
                      decoration: BoxDecoration(
                        color: aicon,
                        borderRadius: BorderRadius.circular(8),
                      ),
                      child: Row(
                        children: [
                          SvgPicture.asset(
                            "assets/icons/item.svg",
                          ),
                          Text(
                            "Add New",
                            style: GoogleFonts.openSans(
                              fontSize: 16,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.all(5),
              color: Color.fromRGBO(251, 212, 254, 1),
              width: double.infinity,
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(8),
                ),
                padding: EdgeInsets.all(10),
                child: Column(
                  children: List.generate(
                    fooditems.length,
                    (index) => InkWell(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => EditMealScreen(),
                          ),
                        );
                      },
                      child: Container(
                        height: 52,
                        padding: EdgeInsets.symmetric(
                          horizontal: 10,
                        ),
                        decoration: BoxDecoration(
                          color: (index == 3)
                              ? Color.fromRGBO(176, 136, 249, 0.1)
                              : Colors.white,
                          borderRadius: BorderRadius.circular(8),
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              fooditems[index],
                            ),
                            IconButton(
                              onPressed: () {},
                              icon: Icon(
                                Icons.add,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              margin: EdgeInsets.all(
                20,
              ),
              width: double.infinity,
              height: 56,
              child: ElevatedButton(
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(
                    aicon,
                  ),
                  // elevation: MaterialStateProperty.all(0),
                ),
                onPressed: () {},
                child: Text(
                  "Save",
                  style: GoogleFonts.openSans(
                    color: unaicon,
                    fontSize: 20,
                    fontWeight: FontWeight.w700,
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
