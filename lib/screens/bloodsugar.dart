import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:luna/model.dart';
import 'package:luna/widgets/activebutton.dart';
import 'package:luna/widgets/timepicker.dart';
import 'package:luna/widgets/togglebutton.dart';

class BloodSugar extends StatefulWidget {
  BloodSugar({
    super.key,
    required this.name,
  });

  final String name;

  @override
  State<BloodSugar> createState() => _BloodSugarState();
}

class _BloodSugarState extends State<BloodSugar> {
  final TextEditingController readingController = TextEditingController();
  final formKey = GlobalKey<FormState>();
  String units = "Mg/DL";
  late DateTime _selectedDate = DateTime.now();
  int hours = 00;
  int minutes = 00;
  bool isAM = true;

  Future<void> _selectDate(BuildContext context) async {
    final DateTime? pickedDate = await showDatePicker(
      context: context,
      initialDate: _selectedDate,
      firstDate: DateTime(0000),
      lastDate: DateTime.now(),
      builder: (context, child) {
        return Theme(
          data: Theme.of(context).copyWith(
            colorScheme: ColorScheme.light(
              primary: purple,
            ),
          ),
          child: child!,
        );
      },
    );

    if (pickedDate != null && pickedDate != _selectedDate) {
      setState(() {
        _selectedDate = pickedDate;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final DateFormat formatter = DateFormat('dd/MM/yyyy');
    return Scaffold(
      appBar: AppBar(
        backgroundColor: background,
        elevation: 0,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(
            Icons.arrow_back_ios_new,
            color: unaicon,
          ),
        ),
        leadingWidth: 30,
        centerTitle: false,
        title: Text(
          widget.name,
          style: GoogleFonts.openSans(
            color: unaicon,
            fontSize: 24,
            fontWeight: FontWeight.w700,
          ),
        ),
      ),
      body: Container(
        padding: EdgeInsets.all(20),
        child: SingleChildScrollView(
          child: Form(
            key: formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                //Sync Data button starts here
                BorderedButton(
                  text: "Sync Data from App",
                  onpressed: () {},
                ),
                //Sync Data button ends here
                SizedBox(
                  height: 28,
                ),
                Text(
                  'Or Enter Manually:',
                  style: GoogleFonts.openSans(
                    color: unaicon,
                    fontSize: 18,
                    fontWeight: FontWeight.w700,
                  ),
                ),
                SizedBox(
                  height: 28,
                ),
                Text(
                  "Reading",
                  style: GoogleFonts.openSans(
                    color: unaicon,
                    fontSize: 16,
                    fontWeight: FontWeight.w400,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 4.0),
                  child: Row(
                    children: [
                      Container(
                        height: 48,
                        width: 142,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          border: Border.all(
                            color: border,
                          ),
                          borderRadius: BorderRadius.circular(8),
                        ),
                        alignment: Alignment.center,
                        child: TextFormField(
                          validator: (value) {
                            if (value!.isEmpty) {
                              showsnackbar(
                                context,
                                red,
                                "value shouldn't be empty",
                              );
                            }
                            return null;
                          },
                          textAlign: TextAlign.center,
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: "00",
                            hintStyle: GoogleFonts.openSans(
                              fontSize: 16,
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                          controller: readingController,
                        ),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Container(
                        height: 48,
                        width: 142,
                        decoration: BoxDecoration(
                          color: box,
                          borderRadius: BorderRadius.circular(8),
                        ),
                        alignment: Alignment.center,
                        child: Text(
                          "Mg/DL",
                        ),
                      ),
                    ],
                  ),
                ),
                //Readings end here
                SizedBox(
                  height: 28,
                ),
                //Units dropdown starts her
                Text(
                  "Units",
                  style: GoogleFonts.openSans(
                    color: unaicon,
                    fontSize: 16,
                    fontWeight: FontWeight.w400,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 4.0),
                  child: Container(
                    height: 48,
                    width: double.infinity,
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: border,
                      ),
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(8),
                    ),
                    child: DropdownButtonFormField(
                      padding: EdgeInsets.symmetric(
                        horizontal: 10,
                      ),
                      decoration: InputDecoration(
                        border: InputBorder.none,
                      ),
                      isExpanded: true,
                      icon: Icon(
                        Icons.keyboard_arrow_down_rounded,
                        color: purple,
                      ),
                      items: const [
                        DropdownMenuItem(
                          value: "1",
                          child: Text(
                            "None",
                          ),
                        ),
                        DropdownMenuItem(
                          value: "2",
                          child: Text(
                            "1",
                          ),
                        ),
                        DropdownMenuItem(
                          value: "Mg/DL",
                          child: Text(
                            "Mg/DL",
                          ),
                        ),
                      ],
                      value: units,
                      onChanged: (value) {
                        setState(() {
                          units = value!;
                        });
                      },
                    ),
                  ),
                ),
                //Units dropdown ends here
                SizedBox(
                  height: 28,
                ),
                //Datepicker starts here
                Text(
                  "Date",
                  style: GoogleFonts.openSans(
                    color: unaicon,
                    fontSize: 16,
                    fontWeight: FontWeight.w400,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 4.0),
                  child: GestureDetector(
                    onTap: () => _selectDate(context),
                    child: Container(
                      height: 48,
                      width: double.infinity,
                      padding: EdgeInsets.all(10.0),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        border: Border.all(
                          color: border,
                          width: 1.0,
                        ),
                        borderRadius: BorderRadius.circular(8.0),
                      ),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            formatter.format(
                                _selectedDate), // Customize the date format as per your requirement
                            style: TextStyle(fontSize: 16.0),
                          ),
                          SvgPicture.asset(
                            "assets/icons/calender.svg",
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                //Datepicker ends here
                SizedBox(
                  height: 28,
                ),
                //Time starts here
                Padding(
                  padding: const EdgeInsets.only(top: 4.0),
                  child: TimePicker(
                    initialValue: isAM,
                    lefttext: "AM",
                    righttext: "PM",
                    onChanged: (value) {
                      setState(() {
                        isAM = value;
                      });
                      print(isAM);
                    },
                    label: "Time",
                  ),
                ),
                //time ends here
                SizedBox(
                  height: 28,
                ),
                //Fasting starts here
                Text(
                  "Fasting",
                  style: GoogleFonts.openSans(
                    color: unaicon,
                    fontSize: 16,
                    fontWeight: FontWeight.w400,
                  ),
                ),

                Padding(
                  padding: const EdgeInsets.only(top: 4.0),
                  child: Row(
                    children: [
                      Container(
                        height: 48,
                        width: 67,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          border: Border.all(
                            color: border,
                          ),
                          borderRadius: BorderRadius.circular(8),
                        ),
                        child: TextFormField(
                          validator: (String? value) {
                            if (value!.isNotEmpty) {
                              final n = int.tryParse(value);
                              if (24 <= n! || n <= 0) {
                                showsnackbar(
                                  context,
                                  red,
                                  "hours should lie between 0 and 24 ",
                                );
                              } else {
                                return null;
                              }
                            }

                            showsnackbar(
                              context,
                              red,
                              "hours shouldn't be empty",
                            );
                          },
                          textAlign: TextAlign.center,
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: "00",
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Container(
                        height: 48,
                        width: 67,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          border: Border.all(
                            color: border,
                          ),
                          borderRadius: BorderRadius.circular(8),
                        ),
                        child: TextFormField(
                          validator: (String? value) {
                            if (value!.isNotEmpty) {
                              final n = int.tryParse(value);
                              if (60 <= n! || n <= 0) {
                                showsnackbar(
                                  context,
                                  red,
                                  "minutes should lie between 0 and 60",
                                );
                              } else {
                                return null;
                              }
                            }

                            showsnackbar(
                              context,
                              red,
                              "minutes shouldn't be empty",
                            );
                          },
                          textAlign: TextAlign.center,
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: "00",
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Container(
                        height: 48,
                        width: 120,
                        decoration: BoxDecoration(
                          color: box1,
                          borderRadius: BorderRadius.circular(8),
                        ),
                        alignment: Alignment.center,
                        child: Text(
                          "Hours",
                          style: GoogleFonts.openSans(
                            fontSize: 16,
                            fontWeight: FontWeight.w400,
                            color: unaicon,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                //Fasting ends here
                SizedBox(
                  height: 28,
                ),
                MainActiveButton(
                  title: "SAVE",
                  onTap: () {},
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
