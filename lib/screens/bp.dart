import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:luna/model.dart';
import 'package:luna/pages.dart';
import 'package:luna/widgets/timepicker.dart';

class BloodPressure extends StatefulWidget {
  const BloodPressure({
    super.key,
    required this.name,
  });

  final String name;

  @override
  State<BloodPressure> createState() => _BloodPressureState();
}

class _BloodPressureState extends State<BloodPressure> {
  String bpunits = "mmHg";
  bool formvalid = false;
  bool isAM = true;
  late DateTime _bpselectedDate = DateTime.now();
  Future<void> _selectDate(BuildContext context) async {
    final DateTime? pickedDate = await showDatePicker(
      context: context,
      initialDate: _bpselectedDate,
      firstDate: DateTime(0000),
      lastDate: DateTime.now(),
      builder: (context, child) {
        return Theme(
          data: Theme.of(context).copyWith(
            colorScheme: ColorScheme.light(
              primary: purple,
            ),
          ),
          child: child!,
        );
      },
    );

    if (pickedDate != null && pickedDate != _bpselectedDate) {
      setState(() {
        _bpselectedDate = pickedDate;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final DateFormat formatter = DateFormat('dd/MM/yyyy');
    final formKey = GlobalKey<FormState>();
    return Scaffold(
      appBar: AppBar(
        backgroundColor: background,
        elevation: 0,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(
            Icons.arrow_back_ios_new,
            color: unaicon,
          ),
        ),
        leadingWidth: 30,
        centerTitle: false,
        title: Text(
          widget.name,
          style: GoogleFonts.openSans(
            color: unaicon,
            fontSize: 24,
            fontWeight: FontWeight.w700,
          ),
        ),
      ),
      body: Container(
        padding: EdgeInsets.all(20),
        child: SingleChildScrollView(
          child: Form(
            key: formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                BorderedButton(
                  text: "Sync Data from App",
                  onpressed: () {},
                ),
                //Sync Data button ends here
                SizedBox(
                  height: 28,
                ),
                Text(
                  'Or Enter Manually:',
                  style: GoogleFonts.openSans(
                    color: unaicon,
                    fontSize: 18,
                    fontWeight: FontWeight.w700,
                  ),
                ),
                SizedBox(
                  height: 28,
                ),
                Text(
                  "Systolic / Diastolic",
                  style: GoogleFonts.openSans(
                    color: unaicon,
                    fontSize: 16,
                    fontWeight: FontWeight.w400,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 8.0),
                  child: Row(
                    children: [
                      Container(
                        height: 48,
                        width: 67,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          border: Border.all(
                            color: border,
                          ),
                          borderRadius: BorderRadius.circular(8),
                        ),
                        child: TextFormField(
                          textAlign: TextAlign.center,
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: "100",
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Container(
                        height: 48,
                        width: 67,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          border: Border.all(
                            color: border,
                          ),
                          borderRadius: BorderRadius.circular(8),
                        ),
                        child: TextFormField(
                          textAlign: TextAlign.center,
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: "100",
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Container(
                        height: 48,
                        width: 120,
                        decoration: BoxDecoration(
                          color: box1,
                          borderRadius: BorderRadius.circular(8),
                        ),
                        alignment: Alignment.center,
                        child: Text(
                          "mmHg",
                          style: GoogleFonts.openSans(
                            fontSize: 16,
                            fontWeight: FontWeight.w400,
                            color: unaicon,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                //S/D ends here
                SizedBox(
                  height: 28,
                ),
                //Units dropdown starts her
                Text(
                  "Units",
                  style: GoogleFonts.openSans(
                    color: unaicon,
                    fontSize: 16,
                    fontWeight: FontWeight.w400,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 8.0),
                  child: Container(
                    height: 48,
                    width: double.infinity,
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: border,
                      ),
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(8),
                    ),
                    child: DropdownButtonFormField(
                      padding: EdgeInsets.symmetric(
                        horizontal: 10,
                      ),
                      decoration: InputDecoration(
                        border: InputBorder.none,
                      ),
                      isExpanded: true,
                      icon: Icon(
                        Icons.keyboard_arrow_down_rounded,
                        color: purple,
                      ),
                      items: const [
                        DropdownMenuItem(
                          value: "1",
                          child: Text(
                            "None",
                          ),
                        ),
                        DropdownMenuItem(
                          value: "2",
                          child: Text(
                            "1",
                          ),
                        ),
                        DropdownMenuItem(
                          value: "mmHg",
                          child: Text(
                            "mmHg",
                          ),
                        ),
                      ],
                      value: bpunits,
                      onChanged: (value) {
                        setState(() {
                          bpunits = value!;
                        });
                      },
                    ),
                  ),
                ),
                SizedBox(
                  height: 28,
                ),
                //Datepicker starts here
                Text(
                  "Date",
                  style: GoogleFonts.openSans(
                    color: unaicon,
                    fontSize: 16,
                    fontWeight: FontWeight.w400,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 8.0),
                  child: GestureDetector(
                    onTap: () => _selectDate(context),
                    child: Container(
                      height: 48,
                      width: double.infinity,
                      padding: EdgeInsets.all(10.0),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        border: Border.all(
                          color: border,
                          width: 1.0,
                        ),
                        borderRadius: BorderRadius.circular(8.0),
                      ),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            formatter.format(
                                _bpselectedDate), // Customize the date format as per your requirement
                            style: TextStyle(fontSize: 16.0),
                          ),
                          SvgPicture.asset(
                            "assets/icons/calender.svg",
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                //Datepicker ends here
                SizedBox(
                  height: 28,
                ),
                //Time starts here
                TimePicker(
                  lefttext: "AM",
                  righttext: "PM",
                  onChanged: (value) {
                    setState(() {
                      isAM = value;
                    });
                    print(isAM);
                  },
                  initialValue: isAM,
                  label: "Time",
                ),
                SizedBox(
                  height: 28,
                ),
                MainActiveButton(
                  title: "SAVE",
                  onTap: () {},
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

// Widget BloodPressure() {
//   return Container(
//     child: Center(
//       child: Text(
//         "hello world",
//       ),
//     ),
//   );
// }
