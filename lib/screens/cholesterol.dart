import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:luna/model.dart';
import 'package:luna/widgets/activebutton.dart';
import 'package:luna/widgets/timepicker.dart';

class Cholesterol extends StatefulWidget {
  const Cholesterol({
    super.key,
    required this.name,
  });

  final String name;

  @override
  State<Cholesterol> createState() => _CholesterolState();
}

class _CholesterolState extends State<Cholesterol> {
  late DateTime _cholesterolselectedDate = DateTime.now();
  bool isAM = true;
  Future<void> _selectDate(BuildContext context) async {
    final DateTime? pickedDate = await showDatePicker(
      context: context,
      initialDate: _cholesterolselectedDate,
      firstDate: DateTime(0000),
      lastDate: DateTime.now(),
      builder: (context, child) {
        return Theme(
          data: Theme.of(context).copyWith(
            colorScheme: ColorScheme.light(
              primary: purple,
            ),
          ),
          child: child!,
        );
      },
    );

    if (pickedDate != null && pickedDate != _cholesterolselectedDate) {
      setState(() {
        _cholesterolselectedDate = pickedDate;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final DateFormat formatter = DateFormat('dd/MM/yyyy');
    return Scaffold(
      appBar: AppBar(
        backgroundColor: background,
        elevation: 0,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(
            Icons.arrow_back_ios_new,
            color: unaicon,
          ),
        ),
        leadingWidth: 30,
        centerTitle: false,
        title: Text(
          widget.name,
          style: TextStyle(
            color: unaicon,
            fontSize: 24,
            fontWeight: FontWeight.w700,
          ),
        ),
      ),
      body: Container(
        padding: EdgeInsets.all(20),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                alignment: Alignment.center,
                height: 56,
                width: double.infinity,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  color: boxinner,
                  border: Border.all(
                    color: border,
                  ),
                ),
                child: Text(
                  "Sync Data from App",
                  style: GoogleFonts.openSans(
                    color: unaicon,
                    fontSize: 16,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
              //Sync Data button ends here
              SizedBox(
                height: 28,
              ),
              Text(
                'Or Enter Manually:',
                style: GoogleFonts.openSans(
                  color: unaicon,
                  fontSize: 18,
                  fontWeight: FontWeight.w700,
                ),
              ),
              //S/D ends here
              SizedBox(
                height: 28,
              ),
              Text(
                "Total Cholesterol",
                style: GoogleFonts.openSans(
                  color: unaicon,
                  fontSize: 16,
                  fontWeight: FontWeight.w400,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 4.0),
                child: Row(
                  children: [
                    Container(
                      height: 48,
                      width: 142,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        border: Border.all(
                          color: border,
                        ),
                        borderRadius: BorderRadius.circular(8),
                      ),
                      child: TextFormField(
                        keyboardType: TextInputType.number,
                        // validator: (value) {
                        //   if (value == null) {
                        //     return "this value shouldn't be empty";
                        //   }
                        //   final n = num.tryParse(value);
                        //   if (60 <= n!) {
                        //     return "should be less 60";
                        //   }
                        //   return null;
                        // },
                        textAlign: TextAlign.center,
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: "50",
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    //Toggle switch starts here
                    Container(
                      height: 48,
                      width: 120,
                      decoration: BoxDecoration(
                        color: box1,
                        borderRadius: BorderRadius.circular(8),
                      ),
                      alignment: Alignment.center,
                      child: Text(
                        "mg/DL",
                        style: GoogleFonts.openSans(
                          fontSize: 16,
                          fontWeight: FontWeight.w400,
                          color: unaicon,
                        ),
                      ),
                    ),
                    //Toggle switch ends here
                  ],
                ),
              ),
              SizedBox(
                height: 28,
              ),
              Text(
                "LDL",
                style: GoogleFonts.openSans(
                  color: unaicon,
                  fontSize: 16,
                  fontWeight: FontWeight.w400,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 4.0),
                child: Row(
                  children: [
                    Container(
                      height: 48,
                      width: 142,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        border: Border.all(
                          color: border,
                        ),
                        borderRadius: BorderRadius.circular(8),
                      ),
                      child: TextFormField(
                        keyboardType: TextInputType.number,
                        // validator: (value) {
                        //   if (value == null) {
                        //     return "this value shouldn't be empty";
                        //   }
                        //   final n = num.tryParse(value);
                        //   if (60 <= n!) {
                        //     return "should be less 60";
                        //   }
                        //   return null;
                        // },
                        textAlign: TextAlign.center,
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: "50",
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    //Toggle switch starts here
                    Container(
                      height: 48,
                      width: 120,
                      decoration: BoxDecoration(
                        color: box1,
                        borderRadius: BorderRadius.circular(8),
                      ),
                      alignment: Alignment.center,
                      child: Text(
                        "mg/DL",
                        style: GoogleFonts.openSans(
                          fontSize: 16,
                          fontWeight: FontWeight.w400,
                          color: unaicon,
                        ),
                      ),
                    ),
                    //Toggle switch ends here
                  ],
                ),
              ),
              SizedBox(
                height: 28,
              ),
              Text(
                "HDL",
                style: GoogleFonts.openSans(
                  color: unaicon,
                  fontSize: 16,
                  fontWeight: FontWeight.w400,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 4.0),
                child: Row(
                  children: [
                    Container(
                      height: 48,
                      width: 142,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        border: Border.all(
                          color: border,
                        ),
                        borderRadius: BorderRadius.circular(8),
                      ),
                      child: TextFormField(
                        keyboardType: TextInputType.number,
                        // validator: (value) {
                        //   if (value == null) {
                        //     return "this value shouldn't be empty";
                        //   }
                        //   final n = num.tryParse(value);
                        //   if (60 <= n!) {
                        //     return "should be less 60";
                        //   }
                        //   return null;
                        // },
                        textAlign: TextAlign.center,
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: "50",
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    //Toggle switch starts here
                    Container(
                      height: 48,
                      width: 120,
                      decoration: BoxDecoration(
                        color: box1,
                        borderRadius: BorderRadius.circular(8),
                      ),
                      alignment: Alignment.center,
                      child: Text(
                        "mg/DL",
                        style: GoogleFonts.openSans(
                          fontSize: 16,
                          fontWeight: FontWeight.w400,
                          color: unaicon,
                        ),
                      ),
                    ),
                    //Toggle switch ends here
                  ],
                ),
              ),
              SizedBox(
                height: 28,
              ),
              Text(
                "Triglycerides",
                style: GoogleFonts.openSans(
                  color: unaicon,
                  fontSize: 16,
                  fontWeight: FontWeight.w400,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 4.0),
                child: Row(
                  children: [
                    Container(
                      height: 48,
                      width: 142,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        border: Border.all(
                          color: border,
                        ),
                        borderRadius: BorderRadius.circular(8),
                      ),
                      child: TextFormField(
                        keyboardType: TextInputType.number,
                        // validator: (value) {
                        //   if (value == null) {
                        //     return "this value shouldn't be empty";
                        //   }
                        //   final n = num.tryParse(value);
                        //   if (60 <= n!) {
                        //     return "should be less 60";
                        //   }
                        //   return null;
                        // },
                        textAlign: TextAlign.center,
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: "50",
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    //Toggle switch starts here
                    Container(
                      height: 48,
                      width: 120,
                      decoration: BoxDecoration(
                        color: box1,
                        borderRadius: BorderRadius.circular(8),
                      ),
                      alignment: Alignment.center,
                      child: Text(
                        "mg/DL",
                        style: GoogleFonts.openSans(
                          fontSize: 16,
                          fontWeight: FontWeight.w400,
                          color: unaicon,
                        ),
                      ),
                    ),
                    //Toggle switch ends here
                  ],
                ),
              ),

              SizedBox(
                height: 28,
              ),
              //Datepicker starts here
              Text(
                "Date",
                style: GoogleFonts.openSans(
                  color: unaicon,
                  fontSize: 16,
                  fontWeight: FontWeight.w400,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 4.0),
                child: GestureDetector(
                  onTap: () => _selectDate(context),
                  child: Container(
                    height: 48,
                    width: double.infinity,
                    padding: EdgeInsets.all(10.0),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      border: Border.all(
                        color: border,
                        width: 1.0,
                      ),
                      borderRadius: BorderRadius.circular(8.0),
                    ),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          formatter.format(
                              _cholesterolselectedDate), // Customize the date format as per your requirement
                          style: TextStyle(fontSize: 16.0),
                        ),
                        SvgPicture.asset(
                          "assets/icons/calender.svg",
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              //Datepicker ends here
              SizedBox(
                height: 28,
              ),
              //Time starts here
              TimePicker(
                lefttext: "AM",
                righttext: "PM",
                onChanged: (value) {
                  setState(() {
                    isAM = value;
                  });
                  print(isAM);
                },
                initialValue: isAM,
                label: "Time",
              ),
              SizedBox(
                height: 28,
              ),
              Text(
                "Fasting",
                style: GoogleFonts.openSans(
                  color: unaicon,
                  fontSize: 16,
                  fontWeight: FontWeight.w400,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 4.0),
                child: Row(
                  children: [
                    Container(
                      height: 48,
                      width: 67,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        border: Border.all(
                          color: border,
                        ),
                        borderRadius: BorderRadius.circular(8),
                      ),
                      child: TextFormField(
                        textAlign: TextAlign.center,
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: "00",
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Container(
                      height: 48,
                      width: 67,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        border: Border.all(
                          color: border,
                        ),
                        borderRadius: BorderRadius.circular(8),
                      ),
                      child: TextFormField(
                        textAlign: TextAlign.center,
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: "00",
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Container(
                      height: 48,
                      width: 120,
                      decoration: BoxDecoration(
                        color: box1,
                        borderRadius: BorderRadius.circular(8),
                      ),
                      alignment: Alignment.center,
                      child: Text(
                        "Hours",
                        style: GoogleFonts.openSans(
                          fontSize: 16,
                          fontWeight: FontWeight.w400,
                          color: unaicon,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 28,
              ),
              MainActiveButton(
                title: "SAVE",
                onTap: () {},
              ),
              SizedBox(
                height: 20,
              ),
            ],
          ),
        ),
      ),
    );
  }
}

// Widget Cholesterol() {
//   return Container(
//     child: Center(
//       child: Text(
//         "hello world",
//       ),
//     ),
//   );
// }
