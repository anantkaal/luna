import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:luna/model.dart';
import 'package:luna/pages.dart';
import 'package:luna/widgets/activebutton.dart';

class Food extends StatefulWidget {
  const Food({
    super.key,
    required this.name,
  });

  final String name;

  @override
  State<Food> createState() => _FoodState();
}

class _FoodState extends State<Food> {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Scaffold(
      appBar: AppBar(
        backgroundColor: background,
        elevation: 0,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(
            Icons.arrow_back_ios_new,
            color: unaicon,
          ),
        ),
        leadingWidth: 30,
        centerTitle: false,
        title: Text(
          widget.name,
          style: TextStyle(
            color: unaicon,
            fontSize: 24,
            fontWeight: FontWeight.w700,
          ),
        ),
      ),
      body: Container(
        padding: EdgeInsets.all(10),
        child: ListView(
          children: [
            //breakfast container starts here
            Container(
              height: 134,
              width: double.infinity,
              padding: EdgeInsets.symmetric(
                horizontal: 15,
                vertical: 16,
              ),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(8),
                border: Border.all(
                  color: border,
                ),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Breakfast",
                    style: GoogleFonts.openSans(
                      color: unaicon,
                      fontWeight: FontWeight.w600,
                      fontSize: 16,
                    ),
                  ),
                  Row(
                    children: [
                      Container(
                        height: 76,
                        width: (size.width - 40) * 0.33,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'Total Calories',
                                  style: GoogleFonts.openSans(
                                    color: grey,
                                    fontSize: 12,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                                Text(
                                  '2000',
                                  style: GoogleFonts.openSans(
                                    color: Color(0xFF303841),
                                    fontSize: 16,
                                    fontWeight: FontWeight.w600,
                                  ),
                                ),
                              ],
                            ),
                            Container(
                              width: 67,
                              height: 22,
                              decoration: BoxDecoration(
                                color: Color(0xFFB088F9),
                                borderRadius: BorderRadius.circular(8),
                              ),
                              child: ElevatedButton(
                                style: ButtonStyle(
                                  backgroundColor: MaterialStateProperty.all(
                                    purple,
                                  ),
                                ),
                                onPressed: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => FoodLogsPage(),
                                    ),
                                  );
                                },
                                child: Text(
                                  "Edit",
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        height: 76,
                        width: (size.width - 40) * 0.3,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Container(
                                  height: 8,
                                  width: 8,
                                  decoration: BoxDecoration(
                                    color: pie1,
                                    shape: BoxShape.circle,
                                  ),
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                Text(
                                  "30% Fats",
                                  style: GoogleFonts.openSans(
                                    color: grey,
                                    fontSize: 12,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ],
                            ),
                            Row(
                              children: [
                                Container(
                                  height: 8,
                                  width: 8,
                                  decoration: BoxDecoration(
                                    color: pie2,
                                    shape: BoxShape.circle,
                                  ),
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                Text(
                                  "20% Other",
                                  style: GoogleFonts.openSans(
                                    color: grey,
                                    fontSize: 12,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ],
                            ),
                            Row(
                              children: [
                                Container(
                                  height: 8,
                                  width: 8,
                                  decoration: BoxDecoration(
                                    color: pie3,
                                    shape: BoxShape.circle,
                                  ),
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                Text(
                                  "20% Proteins",
                                  style: GoogleFonts.openSans(
                                    color: grey,
                                    fontSize: 12,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ],
                            ),
                            Row(
                              children: [
                                Container(
                                  height: 8,
                                  width: 8,
                                  decoration: BoxDecoration(
                                    color: pie4,
                                    shape: BoxShape.circle,
                                  ),
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                Text(
                                  "10% Carbs",
                                  style: GoogleFonts.openSans(
                                    color: grey,
                                    fontSize: 12,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      Container(
                        height: 76,
                        width: (size.width - 40) * 0.33,
                        child: Container(
                          height: 69,
                          width: 69,
                          child: PieChart(
                            PieChartData(
                              sectionsSpace: 0,
                              centerSpaceRadius:
                                  20, // adjust this for doughnut hole size
                              sections: [
                                PieChartSectionData(
                                  color: pie1,
                                  value: 25, // amount of fats
                                  showTitle: false,
                                  radius: 15,
                                ),
                                PieChartSectionData(
                                  radius: 15,
                                  color: pie3,
                                  value: 15,
                                  showTitle: false, // amount of proteins
                                ),
                                PieChartSectionData(
                                  color: pie4,
                                  value: 25,
                                  showTitle: false, // amount of others

                                  radius: 15,
                                ),
                                PieChartSectionData(
                                  color: pie5,
                                  value: 25,
                                  showTitle: false, // amount of others

                                  radius: 15,
                                ),
                                PieChartSectionData(
                                  color: pie2,
                                  value: 35,
                                  // amount of carbs
                                  showTitle: false,
                                  radius: 15,
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            //breakfast container ends here
            SizedBox(
              height: 24,
            ),
            //lunch Container starts here
            Container(
              height: 134,
              width: double.infinity,
              padding: EdgeInsets.symmetric(
                horizontal: 15,
                vertical: 16,
              ),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(8),
                border: Border.all(
                  color: border,
                ),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Lunch",
                    style: GoogleFonts.openSans(
                      color: unaicon,
                      fontWeight: FontWeight.w600,
                      fontSize: 16,
                    ),
                  ),
                  Row(
                    children: [
                      Container(
                        height: 76,
                        width: (size.width - 40) * 0.33,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'Total Calories',
                                  style: GoogleFonts.openSans(
                                    color: grey,
                                    fontSize: 12,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                                Text(
                                  '2000',
                                  style: GoogleFonts.openSans(
                                    color: Color(0xFF303841),
                                    fontSize: 16,
                                    fontWeight: FontWeight.w600,
                                  ),
                                ),
                              ],
                            ),
                            Container(
                              width: 67,
                              height: 22,
                              decoration: BoxDecoration(
                                color: Color(0xFFB088F9),
                                borderRadius: BorderRadius.circular(8),
                              ),
                              child: ElevatedButton(
                                style: ButtonStyle(
                                  backgroundColor: MaterialStateProperty.all(
                                    purple,
                                  ),
                                ),
                                onPressed: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => FoodLogsPage(),
                                    ),
                                  );
                                },
                                child: Text(
                                  "Edit",
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        height: 76,
                        width: (size.width - 40) * 0.3,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Container(
                                  height: 8,
                                  width: 8,
                                  decoration: BoxDecoration(
                                    color: pie1,
                                    shape: BoxShape.circle,
                                  ),
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                Text(
                                  "30% Fats",
                                  style: GoogleFonts.openSans(
                                    color: grey,
                                    fontSize: 12,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ],
                            ),
                            Row(
                              children: [
                                Container(
                                  height: 8,
                                  width: 8,
                                  decoration: BoxDecoration(
                                    color: pie2,
                                    shape: BoxShape.circle,
                                  ),
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                Text(
                                  "20% Other",
                                  style: GoogleFonts.openSans(
                                    color: grey,
                                    fontSize: 12,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ],
                            ),
                            Row(
                              children: [
                                Container(
                                  height: 8,
                                  width: 8,
                                  decoration: BoxDecoration(
                                    color: pie3,
                                    shape: BoxShape.circle,
                                  ),
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                Text(
                                  "20% Proteins",
                                  style: GoogleFonts.openSans(
                                    color: grey,
                                    fontSize: 12,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ],
                            ),
                            Row(
                              children: [
                                Container(
                                  height: 8,
                                  width: 8,
                                  decoration: BoxDecoration(
                                    color: pie4,
                                    shape: BoxShape.circle,
                                  ),
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                Text(
                                  "10% Carbs",
                                  style: GoogleFonts.openSans(
                                    color: grey,
                                    fontSize: 12,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      Container(
                        height: 76,
                        width: (size.width - 40) * 0.33,
                        child: Container(
                          height: 69,
                          width: 69,
                          child: PieChart(
                            PieChartData(
                              sectionsSpace: 0,
                              centerSpaceRadius:
                                  20, // adjust this for doughnut hole size
                              sections: [
                                PieChartSectionData(
                                  color: pie1,
                                  value: 25, // amount of fats
                                  showTitle: false,
                                  radius: 15,
                                ),
                                PieChartSectionData(
                                  radius: 15,
                                  color: pie3,
                                  value: 15,
                                  showTitle: false, // amount of proteins
                                ),
                                PieChartSectionData(
                                  color: pie4,
                                  value: 25,
                                  showTitle: false, // amount of others

                                  radius: 15,
                                ),
                                PieChartSectionData(
                                  color: pie5,
                                  value: 25,
                                  showTitle: false, // amount of others

                                  radius: 15,
                                ),
                                PieChartSectionData(
                                  color: pie2,
                                  value: 35,
                                  // amount of carbs
                                  showTitle: false,
                                  radius: 15,
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            //Lunch container ends here
            SizedBox(
              height: 24,
            ),
            //Snacks Container starts here
            Container(
              height: 134,
              width: double.infinity,
              padding: EdgeInsets.symmetric(
                horizontal: 15,
                vertical: 16,
              ),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(8),
                border: Border.all(
                  color: border,
                ),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Snacks",
                    style: GoogleFonts.openSans(
                      color: unaicon,
                      fontWeight: FontWeight.w600,
                      fontSize: 16,
                    ),
                  ),
                  Row(
                    children: [
                      Container(
                        height: 76,
                        width: (size.width - 40) * 0.33,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'Total Calories',
                                  style: GoogleFonts.openSans(
                                    color: grey,
                                    fontSize: 12,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                                Text(
                                  '2000',
                                  style: GoogleFonts.openSans(
                                    color: Color(0xFF303841),
                                    fontSize: 16,
                                    fontWeight: FontWeight.w600,
                                  ),
                                ),
                              ],
                            ),
                            Container(
                              width: 67,
                              height: 22,
                              decoration: BoxDecoration(
                                color: Color(0xFFB088F9),
                                borderRadius: BorderRadius.circular(8),
                              ),
                              child: ElevatedButton(
                                style: ButtonStyle(
                                  backgroundColor: MaterialStateProperty.all(
                                    purple,
                                  ),
                                ),
                                onPressed: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => FoodLogsPage(),
                                    ),
                                  );
                                },
                                child: Text(
                                  "Edit",
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        height: 76,
                        width: (size.width - 40) * 0.3,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Container(
                                  height: 8,
                                  width: 8,
                                  decoration: BoxDecoration(
                                    color: pie1,
                                    shape: BoxShape.circle,
                                  ),
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                Text(
                                  "30% Fats",
                                  style: GoogleFonts.openSans(
                                    color: grey,
                                    fontSize: 12,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ],
                            ),
                            Row(
                              children: [
                                Container(
                                  height: 8,
                                  width: 8,
                                  decoration: BoxDecoration(
                                    color: pie2,
                                    shape: BoxShape.circle,
                                  ),
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                Text(
                                  "20% Other",
                                  style: GoogleFonts.openSans(
                                    color: grey,
                                    fontSize: 12,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ],
                            ),
                            Row(
                              children: [
                                Container(
                                  height: 8,
                                  width: 8,
                                  decoration: BoxDecoration(
                                    color: pie3,
                                    shape: BoxShape.circle,
                                  ),
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                Text(
                                  "20% Proteins",
                                  style: GoogleFonts.openSans(
                                    color: grey,
                                    fontSize: 12,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ],
                            ),
                            Row(
                              children: [
                                Container(
                                  height: 8,
                                  width: 8,
                                  decoration: BoxDecoration(
                                    color: pie4,
                                    shape: BoxShape.circle,
                                  ),
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                Text(
                                  "10% Carbs",
                                  style: GoogleFonts.openSans(
                                    color: grey,
                                    fontSize: 12,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      Container(
                        height: 76,
                        width: (size.width - 40) * 0.33,
                        child: Container(
                          height: 69,
                          width: 69,
                          child: PieChart(
                            PieChartData(
                              sectionsSpace: 0,
                              centerSpaceRadius:
                                  20, // adjust this for doughnut hole size
                              sections: [
                                PieChartSectionData(
                                  color: pie1,
                                  value: 25, // amount of fats
                                  showTitle: false,
                                  radius: 15,
                                ),
                                PieChartSectionData(
                                  radius: 15,
                                  color: pie3,
                                  value: 15,
                                  showTitle: false, // amount of proteins
                                ),
                                PieChartSectionData(
                                  color: pie4,
                                  value: 25,
                                  showTitle: false, // amount of others

                                  radius: 15,
                                ),
                                PieChartSectionData(
                                  color: pie5,
                                  value: 25,
                                  showTitle: false, // amount of others

                                  radius: 15,
                                ),
                                PieChartSectionData(
                                  color: pie2,
                                  value: 35,
                                  // amount of carbs
                                  showTitle: false,
                                  radius: 15,
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            //Snacks container ends here
            SizedBox(
              height: 24,
            ),
            //Dinner Container starts here
            Container(
              height: 134,
              width: double.infinity,
              padding: EdgeInsets.symmetric(
                horizontal: 15,
                vertical: 16,
              ),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(8),
                border: Border.all(
                  color: border,
                ),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Dinner",
                    style: GoogleFonts.openSans(
                      color: unaicon,
                      fontWeight: FontWeight.w600,
                      fontSize: 16,
                    ),
                  ),
                  Row(
                    children: [
                      Container(
                        height: 76,
                        width: (size.width - 40) * 0.33,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'Total Calories',
                                  style: GoogleFonts.openSans(
                                    color: grey,
                                    fontSize: 12,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                                Text(
                                  '2000',
                                  style: GoogleFonts.openSans(
                                    color: Color(0xFF303841),
                                    fontSize: 16,
                                    fontWeight: FontWeight.w600,
                                  ),
                                ),
                              ],
                            ),
                            Container(
                              width: 67,
                              height: 22,
                              decoration: BoxDecoration(
                                color: Color(0xFFB088F9),
                                borderRadius: BorderRadius.circular(8),
                              ),
                              child: ElevatedButton(
                                style: ButtonStyle(
                                  backgroundColor: MaterialStateProperty.all(
                                    purple,
                                  ),
                                ),
                                onPressed: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => FoodLogsPage(),
                                    ),
                                  );
                                },
                                child: Text(
                                  "Edit",
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        height: 76,
                        width: (size.width - 40) * 0.3,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Container(
                                  height: 8,
                                  width: 8,
                                  decoration: BoxDecoration(
                                    color: pie1,
                                    shape: BoxShape.circle,
                                  ),
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                Text(
                                  "30% Fats",
                                  style: GoogleFonts.openSans(
                                    color: grey,
                                    fontSize: 12,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ],
                            ),
                            Row(
                              children: [
                                Container(
                                  height: 8,
                                  width: 8,
                                  decoration: BoxDecoration(
                                    color: pie2,
                                    shape: BoxShape.circle,
                                  ),
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                Text(
                                  "20% Other",
                                  style: GoogleFonts.openSans(
                                    color: grey,
                                    fontSize: 12,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ],
                            ),
                            Row(
                              children: [
                                Container(
                                  height: 8,
                                  width: 8,
                                  decoration: BoxDecoration(
                                    color: pie3,
                                    shape: BoxShape.circle,
                                  ),
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                Text(
                                  "20% Proteins",
                                  style: GoogleFonts.openSans(
                                    color: grey,
                                    fontSize: 12,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ],
                            ),
                            Row(
                              children: [
                                Container(
                                  height: 8,
                                  width: 8,
                                  decoration: BoxDecoration(
                                    color: pie4,
                                    shape: BoxShape.circle,
                                  ),
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                Text(
                                  "10% Carbs",
                                  style: GoogleFonts.openSans(
                                    color: grey,
                                    fontSize: 12,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      Container(
                        height: 76,
                        width: (size.width - 40) * 0.33,
                        child: Container(
                          height: 69,
                          width: 69,
                          child: PieChart(
                            PieChartData(
                              sectionsSpace: 0,
                              centerSpaceRadius:
                                  20, // adjust this for doughnut hole size
                              sections: [
                                PieChartSectionData(
                                  color: pie1,
                                  value: 25, // amount of fats
                                  showTitle: false,
                                  radius: 15,
                                ),
                                PieChartSectionData(
                                  radius: 15,
                                  color: pie3,
                                  value: 15,
                                  showTitle: false, // amount of proteins
                                ),
                                PieChartSectionData(
                                  color: pie4,
                                  value: 25,
                                  showTitle: false, // amount of others

                                  radius: 15,
                                ),
                                PieChartSectionData(
                                  color: pie5,
                                  value: 25,
                                  showTitle: false, // amount of others

                                  radius: 15,
                                ),
                                PieChartSectionData(
                                  color: pie2,
                                  value: 35,
                                  // amount of carbs
                                  showTitle: false,
                                  radius: 15,
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            //Dinner container ends here
            SizedBox(
              height: 24,
            ),
            MainActiveButton(
              title: "UPDATE",
              onTap: () {},
            ),
          ],
        ),
      ),
    );
  }
}



// Widget Food() {
//   return Container(
//     child: Center(
//       child: Text(
//         "hello world",
//       ),
//     ),
//   );
// }
