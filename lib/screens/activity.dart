import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:luna/model.dart';
import 'package:luna/widgets/activebutton.dart';

class Activity extends StatefulWidget {
  const Activity({
    super.key,
    required this.name,
  });

  final String name;

  @override
  State<Activity> createState() => _ActivityState();
}

class _ActivityState extends State<Activity> {
  String activity = "Aerobic (Intense)";
  bool isAM = true;
  bool isKM = false;

  late DateTime _bpselectedDate = DateTime.now();
  Future<void> _selectDate(BuildContext context) async {
    final DateTime? pickedDate = await showDatePicker(
      context: context,
      initialDate: _bpselectedDate,
      firstDate: DateTime(0000),
      lastDate: DateTime.now(),
      builder: (context, child) {
        return Theme(
          data: Theme.of(context).copyWith(
            colorScheme: ColorScheme.light(
              primary: purple,
            ),
          ),
          child: child!,
        );
      },
    );

    if (pickedDate != null && pickedDate != _bpselectedDate) {
      setState(() {
        _bpselectedDate = pickedDate;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final DateFormat formatter = DateFormat('dd/MM/yyyy');
    return Scaffold(
      appBar: AppBar(
        backgroundColor: background,
        elevation: 0,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(
            Icons.arrow_back_ios_new,
            color: unaicon,
          ),
        ),
        leadingWidth: 30,
        centerTitle: false,
        title: Text(
          widget.name,
          style: TextStyle(
            color: unaicon,
            fontSize: 24,
            fontWeight: FontWeight.w700,
          ),
        ),
      ),
      body: Container(
        padding: EdgeInsets.all(20),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              BorderedButton(
                text: "Sync Data from App",
                onpressed: () {},
              ),
              //Sync Data button ends here
              SizedBox(
                height: 28,
              ),
              Text(
                'Or Enter Manually:',
                style: GoogleFonts.openSans(
                  color: unaicon,
                  fontSize: 18,
                  fontWeight: FontWeight.w700,
                ),
              ),
              SizedBox(
                height: 28,
              ),
              Text(
                "Activity Type",
                style: GoogleFonts.openSans(
                  color: unaicon,
                  fontSize: 16,
                  fontWeight: FontWeight.w400,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 4.0),
                child: Container(
                  height: 48,
                  width: double.infinity,
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: border,
                    ),
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(8),
                  ),
                  child: DropdownButtonFormField(
                    padding: EdgeInsets.symmetric(
                      horizontal: 10,
                    ),
                    decoration: InputDecoration(
                      border: InputBorder.none,
                    ),
                    isExpanded: true,
                    icon: Icon(
                      Icons.keyboard_arrow_down_rounded,
                      color: purple,
                    ),
                    items: const [
                      DropdownMenuItem(
                        value: "Aerobic (Intense)",
                        child: Text(
                          "Aerobic (Intense)",
                        ),
                      ),
                      DropdownMenuItem(
                        value: "Aerobic (Medium)",
                        child: Text(
                          "Aerobic (Medium)",
                        ),
                      ),
                      DropdownMenuItem(
                        value: "Sports (Intense)",
                        child: Text(
                          "Sports (Intense)",
                        ),
                      ),
                      DropdownMenuItem(
                        value: "SSports (Medium)",
                        child: Text(
                          "Sports (Medium)",
                        ),
                      ),
                      DropdownMenuItem(
                        value: "Walking (Brisk)",
                        child: Text(
                          "Walking (Brisk)",
                        ),
                      ),
                      DropdownMenuItem(
                        value: "Walking (Slow)",
                        child: Text(
                          "Walking (Slow)",
                        ),
                      ),
                      DropdownMenuItem(
                        value: "Weight training (Heavy)",
                        child: Text(
                          "Weight training (Heavy)",
                        ),
                      ),
                      DropdownMenuItem(
                        value: "Weight training (Medium)",
                        child: Text(
                          "Weight training (Medium)",
                        ),
                      ),
                    ],
                    value: activity,
                    onChanged: (value) {
                      setState(() {
                        activity = value!;
                      });
                    },
                  ),
                ),
              ),
              SizedBox(
                height: 28,
              ),
              //Datepicker starts here
              Text(
                "Date",
                style: GoogleFonts.openSans(
                  color: unaicon,
                  fontSize: 16,
                  fontWeight: FontWeight.w400,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 4.0),
                child: GestureDetector(
                  onTap: () => _selectDate(context),
                  child: Container(
                    height: 48,
                    width: double.infinity,
                    padding: EdgeInsets.all(10.0),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      border: Border.all(
                        color: border,
                        width: 1.0,
                      ),
                      borderRadius: BorderRadius.circular(8.0),
                    ),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          formatter.format(
                              _bpselectedDate), // Customize the date format as per your requirement
                          style: TextStyle(fontSize: 16.0),
                        ),
                        SvgPicture.asset(
                          "assets/icons/calender.svg",
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 28,
              ),
              Text(
                "Duration of Activity",
                style: GoogleFonts.openSans(
                  color: unaicon,
                  fontSize: 16,
                  fontWeight: FontWeight.w400,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 4.0),
                child: Row(
                  children: [
                    Container(
                      height: 48,
                      width: 67,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        border: Border.all(
                          color: border,
                        ),
                        borderRadius: BorderRadius.circular(8),
                      ),
                      child: TextFormField(
                        textAlign: TextAlign.center,
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: "00",
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Container(
                      height: 48,
                      width: 67,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        border: Border.all(
                          color: border,
                        ),
                        borderRadius: BorderRadius.circular(8),
                      ),
                      child: TextFormField(
                        textAlign: TextAlign.center,
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: "00",
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Container(
                      height: 48,
                      width: 120,
                      decoration: BoxDecoration(
                        color: box1,
                        borderRadius: BorderRadius.circular(8),
                      ),
                      alignment: Alignment.center,
                      child: Text(
                        "Hours",
                        style: GoogleFonts.openSans(
                          fontSize: 16,
                          fontWeight: FontWeight.w400,
                          color: unaicon,
                        ),
                      ),
                    ),
                  ],
                ),
              ),

              SizedBox(
                height: 28,
              ),
              //Time starts here
              Text(
                "Time synced",
                style: GoogleFonts.openSans(
                  color: unaicon,
                  fontSize: 16,
                  fontWeight: FontWeight.w400,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 4.0),
                child: Row(
                  children: [
                    Container(
                      height: 48,
                      width: 67,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        border: Border.all(
                          color: border,
                        ),
                        borderRadius: BorderRadius.circular(8),
                      ),
                      child: TextFormField(
                        keyboardType: TextInputType.number,
                        validator: (value) {
                          if (value == null) {
                            return "this value shouldn't be empty";
                          }
                          final n = num.tryParse(value);
                          if (60 <= n!) {
                            return "should be less 60";
                          }
                          return null;
                        },
                        textAlign: TextAlign.center,
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: "00",
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Container(
                      height: 48,
                      width: 67,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        border: Border.all(
                          color: border,
                        ),
                        borderRadius: BorderRadius.circular(8),
                      ),
                      child: TextFormField(
                        textAlign: TextAlign.center,
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: "00",
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    //Toggle switch starts here
                    Container(
                      height: 48,
                      width: 120,
                      decoration: BoxDecoration(
                        color: box1,
                        borderRadius: BorderRadius.circular(8),
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          GestureDetector(
                            onTap: () {
                              setState(() {
                                isAM = true;
                              });
                            },
                            child: Container(
                              width: 37,
                              height: 28,
                              alignment: Alignment.center,
                              decoration: BoxDecoration(
                                color: isAM ? purple : Colors.transparent,
                                borderRadius: BorderRadius.circular(8),
                              ),
                              child: Text(
                                "AM",
                                maxLines: 1,
                                textAlign: TextAlign.center,
                                style: GoogleFonts.openSans(
                                  fontSize: 16,
                                  fontWeight: FontWeight.w400,
                                  color: isAM ? Colors.white : unaicon,
                                ),
                              ),
                            ),
                          ),
                          GestureDetector(
                            onTap: () {
                              setState(() {
                                isAM = false;
                              });
                            },
                            child: Container(
                              width: 37,
                              height: 28,
                              alignment: Alignment.center,
                              decoration: BoxDecoration(
                                color: isAM ? Colors.transparent : purple,
                                borderRadius: BorderRadius.circular(8),
                              ),
                              child: Text(
                                textAlign: TextAlign.center,
                                maxLines: 1,
                                "PM",
                                style: GoogleFonts.openSans(
                                  fontSize: 16,
                                  fontWeight: FontWeight.w400,
                                  color: isAM ? unaicon : Colors.white,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    //Toggle switch ends here
                  ],
                ),
              ),
              SizedBox(
                height: 28,
              ),
              Text(
                "Distance",
                style: GoogleFonts.openSans(
                  color: unaicon,
                  fontSize: 16,
                  fontWeight: FontWeight.w400,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 4.0),
                child: Row(
                  children: [
                    Container(
                      height: 48,
                      width: 142,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        border: Border.all(
                          color: border,
                        ),
                        borderRadius: BorderRadius.circular(8),
                      ),
                      child: TextFormField(
                        keyboardType: TextInputType.number,
                        // validator: (value) {
                        //   if (value == null) {
                        //     return "this value shouldn't be empty";
                        //   }
                        //   final n = num.tryParse(value);
                        //   if (60 <= n!) {
                        //     return "should be less 60";
                        //   }
                        //   return null;
                        // },
                        textAlign: TextAlign.center,
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: "100",
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    //Toggle switch starts here
                    Container(
                      height: 48,
                      width: 120,
                      decoration: BoxDecoration(
                        color: box1,
                        borderRadius: BorderRadius.circular(8),
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          GestureDetector(
                            onTap: () {
                              setState(() {
                                isKM = true;
                              });
                            },
                            child: Container(
                              width: 50,
                              height: 28,
                              alignment: Alignment.center,
                              decoration: BoxDecoration(
                                color: isKM ? purple : Colors.transparent,
                                borderRadius: BorderRadius.circular(8),
                              ),
                              child: Text(
                                "KM",
                                maxLines: 1,
                                textAlign: TextAlign.center,
                                style: GoogleFonts.openSans(
                                  fontSize: 16,
                                  fontWeight: FontWeight.w400,
                                  color: isKM ? Colors.white : unaicon,
                                ),
                              ),
                            ),
                          ),
                          GestureDetector(
                            onTap: () {
                              setState(() {
                                isKM = false;
                              });
                            },
                            child: Container(
                              width: 50,
                              height: 28,
                              alignment: Alignment.center,
                              decoration: BoxDecoration(
                                color: isKM ? Colors.transparent : purple,
                                borderRadius: BorderRadius.circular(8),
                              ),
                              child: Text(
                                textAlign: TextAlign.center,
                                maxLines: 1,
                                "Miles",
                                style: GoogleFonts.openSans(
                                  fontSize: 14,
                                  fontWeight: FontWeight.w400,
                                  color: isKM ? unaicon : Colors.white,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    //Toggle switch ends here
                  ],
                ),
              ),
              SizedBox(
                height: 28,
              ),
              MainActiveButton(
                title: "SAVE",
                onTap: () {},
              ),
            ],
          ),
        ),
      ),
    );
  }
}

// Widget Activity() {
//   return Container(
//     child: Center(
//       child: Text(
//         "hello world",
//       ),
//     ),
//   );
// }
