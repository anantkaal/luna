import 'package:flutter/material.dart';
import 'package:luna/model.dart';

class MindFullNess extends StatelessWidget {
  const MindFullNess({
    super.key,
    required this.name,
  });

  final String name;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: background,
        elevation: 0,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(
            Icons.arrow_back_ios_new,
            color: unaicon,
          ),
        ),
        leadingWidth: 30,
        centerTitle: false,
        title: Text(
          name,
          style: TextStyle(
            color: unaicon,
            fontSize: 24,
            fontWeight: FontWeight.w700,
          ),
        ),
      ),
      body: Center(
        child: Text(
          "center",
        ),
      ),
    );
  }
}

// Widget MindFullNess() {
//   return Container(
//     child: Center(
//       child: Text(
//         "hello world",
//       ),
//     ),
//   );
// }
