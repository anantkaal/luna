import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:luna/model.dart';
import 'package:luna/widgets/activebutton.dart';

class Plan extends StatefulWidget {
  const Plan({super.key});

  @override
  State<Plan> createState() => _PlanState();
}

class _PlanState extends State<Plan> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.all(20),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Manual Tracking",
                style: GoogleFonts.openSans(
                  color: unaicon,
                  fontSize: 25,
                  fontWeight: FontWeight.w700,
                ),
              ),
              SizedBox(
                height: 30,
              ),
              GridView.builder(
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 3,
                  mainAxisSpacing: 15,
                  crossAxisSpacing: 15,
                  childAspectRatio: 2 / 2,
                ),
                // physics: ScrollPhysics(),
                clipBehavior: Clip.hardEdge,
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemCount: metrices.length,

                itemBuilder: (context, index) => GestureDetector(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => metrices[index]['navigation'],
                      ),
                    );
                  },
                  child: Container(
                    height: 100,
                    width: 100,
                    decoration: BoxDecoration(
                      color: box,
                      borderRadius: BorderRadius.circular(
                        8,
                      ),
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        SizedBox(
                          height: 50,
                          child: SvgPicture.asset(
                            metrices[index]['icon'],
                            fit: BoxFit.fitWidth,
                          ),
                        ),
                        Text(
                          metrices[index]["title"],
                          textAlign: TextAlign.center,
                          style: GoogleFonts.openSans(
                            color: unaicon,
                            fontSize: 14,
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 30,
              ),
              MainActiveButton(
                title: "UPDATE",
                onTap: () {},
              ),
            ],
          ),
        ),
      ),
    );
  }
}
