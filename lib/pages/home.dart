import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:luna/model.dart';
import 'package:luna/widgets/activebutton.dart';
import 'package:luna/widgets/bargraph.dart';

class Home extends StatefulWidget {
  const Home({super.key});

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  bool _isExpanded = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      child: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.all(10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(5.0),
                    child: Text(
                      "Start your day with Inspiration:",
                      style: GoogleFonts.openSans(
                        color: Color(0xFF3A4750),
                        fontSize: 16,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ),
                  Stack(
                    children: [
                      Container(
                        height: 180,
                        width: double.infinity,
                        margin: EdgeInsets.symmetric(horizontal: 10),
                        decoration: BoxDecoration(
                          color: purple,
                          borderRadius: BorderRadius.circular(8),
                          image: DecorationImage(
                              image: AssetImage(
                                "assets/images/corousel.png",
                              ),
                              fit: BoxFit.fill),
                        ),
                      ),
                      Container(
                        height: 180,
                        width: double.infinity,
                        margin: EdgeInsets.symmetric(horizontal: 10),
                        decoration: BoxDecoration(
                          color: purple.withOpacity(0.7),
                          borderRadius: BorderRadius.circular(8),
                        ),
                        alignment: Alignment.center,
                        child: Container(
                          height: 40,
                          width: 116,
                          child: ElevatedButton(
                            style: ButtonStyle(
                              // maximumSize: MaterialStateProperty.all(
                              //   Size(110, 30),
                              // ),
                              padding: MaterialStateProperty.all(
                                EdgeInsets.zero,
                              ),
                              backgroundColor: MaterialStateProperty.all(
                                aicon,
                              ),
                            ),
                            onPressed: () {},
                            child: Text(
                              "Watch now >>",
                              style: GoogleFonts.openSans(
                                color: unaicon,
                                fontSize: 14,
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Container(
              decoration: BoxDecoration(
                color: green,
              ),
              padding: EdgeInsets.all(16),
              child: Column(
                children: [
                  Row(
                    children: [
                      Expanded(
                        child: Text(
                          "Snapshot",
                          style: GoogleFonts.openSans(
                            color: unaicon,
                            fontSize: 24,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),
                      IconButton(
                        icon: _isExpanded
                            ? SvgPicture.asset("assets/icons/collapsearrow.svg")
                            : SvgPicture.asset("assets/icons/expandarrrow.svg"),
                        onPressed: () {
                          setState(() {
                            _isExpanded = !_isExpanded;
                          });
                        },
                      ),
                    ],
                  ),
                  Divider(),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        height: MediaQuery.of(context).size.width * 0.20,
                        width: MediaQuery.of(context).size.width * 0.22,
                        padding: EdgeInsets.all(10),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(8),
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                SizedBox(
                                  height: 24,
                                  width: 24,
                                  child: SvgPicture.asset(
                                    "assets/icons/homeweight.svg",
                                  ),
                                ),
                                Text(
                                  "Weight",
                                  style: GoogleFonts.openSans(
                                    color: unaicon,
                                    fontSize: 14,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ],
                            ),
                            Text(
                              '80 KG',
                              style: GoogleFonts.openSans(
                                color: Color(0xFF303841),
                                fontSize: 18,
                                fontWeight: FontWeight.w600,
                              ),
                            )
                          ],
                        ),
                      ),
                      Container(
                        height: MediaQuery.of(context).size.width * 0.20,
                        width: MediaQuery.of(context).size.width * 0.22,
                        padding: EdgeInsets.all(10),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(8),
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                SizedBox(
                                  height: 24,
                                  width: 24,
                                  child: SvgPicture.asset(
                                    "assets/icons/bmi.svg",
                                  ),
                                ),
                                Text(
                                  "BMI",
                                  style: GoogleFonts.openSans(
                                    color: unaicon,
                                    fontSize: 14,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ],
                            ),
                            Text(
                              '20',
                              style: GoogleFonts.openSans(
                                color: Color(0xFF303841),
                                fontSize: 18,
                                fontWeight: FontWeight.w600,
                              ),
                            )
                          ],
                        ),
                      ),
                      Container(
                        height: MediaQuery.of(context).size.width * 0.20,
                        width: MediaQuery.of(context).size.width * 0.22,
                        padding: EdgeInsets.all(10),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(8),
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                SizedBox(
                                  height: 24,
                                  width: 24,
                                  child: SvgPicture.asset(
                                    "assets/icons/homea1c.svg",
                                  ),
                                ),
                                SizedBox(
                                  width: 5,
                                ),
                                Text(
                                  "A1C",
                                  style: GoogleFonts.openSans(
                                    color: unaicon,
                                    fontSize: 14,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ],
                            ),
                            Text(
                              '7.2%',
                              style: GoogleFonts.openSans(
                                color: Color(0xFF303841),
                                fontSize: 18,
                                fontWeight: FontWeight.w600,
                              ),
                            )
                          ],
                        ),
                      ),
                      Container(
                        height: MediaQuery.of(context).size.width * 0.20,
                        width: MediaQuery.of(context).size.width * 0.22,
                        padding: EdgeInsets.all(10),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(8),
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                SizedBox(
                                  height: 24,
                                  width: 24,
                                  child: SvgPicture.asset(
                                    "assets/icons/healthindex.svg",
                                  ),
                                ),
                                SizedBox(
                                  width: 5,
                                ),
                                Flexible(
                                  child: Text(
                                    "Health Index",
                                    maxLines: 2,
                                    style: GoogleFonts.openSans(
                                      color: unaicon,
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            Text(
                              '15',
                              style: GoogleFonts.openSans(
                                color: Color(0xFF303841),
                                fontSize: 18,
                                fontWeight: FontWeight.w600,
                              ),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                  AnimatedContainer(
                    duration: Duration(milliseconds: 300),
                    height: _isExpanded ? 270 : 0,
                    child: ListView(
                      shrinkWrap: true,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(top: 10.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                height:
                                    MediaQuery.of(context).size.width * 0.20,
                                width: MediaQuery.of(context).size.width * 0.22,
                                padding: EdgeInsets.all(10),
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(8),
                                ),
                                child: Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceEvenly,
                                      children: [
                                        SizedBox(
                                          height: 24,
                                          width: 24,
                                          child: SvgPicture.asset(
                                            "assets/icons/activitybold.svg",
                                          ),
                                        ),
                                        Text(
                                          "Activity",
                                          style: GoogleFonts.openSans(
                                            color: unaicon,
                                            fontSize: 14,
                                            fontWeight: FontWeight.w400,
                                          ),
                                        ),
                                      ],
                                    ),
                                    Text(
                                      '1.5 H',
                                      style: GoogleFonts.openSans(
                                        color: Color(0xFF303841),
                                        fontSize: 18,
                                        fontWeight: FontWeight.w600,
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              Container(
                                height:
                                    MediaQuery.of(context).size.width * 0.20,
                                width: MediaQuery.of(context).size.width * 0.22,
                                padding: EdgeInsets.all(10),
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(8),
                                ),
                                child: Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        SizedBox(
                                          height: 24,
                                          width: 24,
                                          child: SvgPicture.asset(
                                            "assets/icons/sleepbold.svg",
                                          ),
                                        ),
                                        SizedBox(
                                          width: 5,
                                        ),
                                        Text(
                                          "Sleep",
                                          style: GoogleFonts.openSans(
                                            color: unaicon,
                                            fontSize: 14,
                                            fontWeight: FontWeight.w400,
                                          ),
                                        ),
                                      ],
                                    ),
                                    Text(
                                      '8.5 H',
                                      style: GoogleFonts.openSans(
                                        color: Color(0xFF303841),
                                        fontSize: 18,
                                        fontWeight: FontWeight.w600,
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              Container(
                                height:
                                    MediaQuery.of(context).size.width * 0.20,
                                width: MediaQuery.of(context).size.width * 0.22,
                                padding: EdgeInsets.symmetric(
                                    vertical: 10, horizontal: 7),
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(8),
                                ),
                                child: Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        SizedBox(
                                          height: 24,
                                          width: 24,
                                          child: SvgPicture.asset(
                                            "assets/icons/mindfullbold.svg",
                                          ),
                                        ),
                                        SizedBox(
                                          width: 5,
                                        ),
                                        Flexible(
                                          child: Text(
                                            "Mindful",
                                            style: GoogleFonts.openSans(
                                              color: unaicon,
                                              fontSize: 14,
                                              fontWeight: FontWeight.w400,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                    Text(
                                      '30 M',
                                      style: GoogleFonts.openSans(
                                        color: Color(0xFF303841),
                                        fontSize: 18,
                                        fontWeight: FontWeight.w600,
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              Container(
                                height:
                                    MediaQuery.of(context).size.width * 0.20,
                                width: MediaQuery.of(context).size.width * 0.22,
                                padding: EdgeInsets.symmetric(
                                    vertical: 10, horizontal: 7),
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(8),
                                ),
                                child: Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceEvenly,
                                      children: [
                                        SizedBox(
                                          height: 24,
                                          width: 24,
                                          child: SvgPicture.asset(
                                            "assets/icons/bsbold.svg",
                                          ),
                                        ),
                                        SizedBox(
                                          width: 5,
                                        ),
                                        Flexible(
                                          child: Text(
                                            "Blood Sugar",
                                            maxLines: 2,
                                            style: GoogleFonts.openSans(
                                              color: unaicon,
                                              fontSize: 14,
                                              fontWeight: FontWeight.w400,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                    Text(
                                      '15',
                                      style: GoogleFonts.openSans(
                                        color: Color(0xFF303841),
                                        fontSize: 18,
                                        fontWeight: FontWeight.w600,
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 10.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                height:
                                    MediaQuery.of(context).size.width * 0.20,
                                width: MediaQuery.of(context).size.width * 0.22,
                                padding: EdgeInsets.symmetric(
                                    vertical: 10, horizontal: 7),
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(8),
                                ),
                                child: Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceEvenly,
                                      children: [
                                        SizedBox(
                                          height: 24,
                                          width: 24,
                                          child: SvgPicture.asset(
                                            "assets/icons/BPbold.svg",
                                          ),
                                        ),
                                        Flexible(
                                          child: Text(
                                            "Blood Pressure",
                                            maxLines: 2,
                                            style: GoogleFonts.openSans(
                                              color: unaicon,
                                              fontSize: 14,
                                              fontWeight: FontWeight.w400,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                    Text(
                                      '120/120',
                                      style: GoogleFonts.openSans(
                                        color: Color(0xFF303841),
                                        fontSize: 18,
                                        fontWeight: FontWeight.w600,
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              Container(
                                height:
                                    MediaQuery.of(context).size.width * 0.20,
                                width: MediaQuery.of(context).size.width * 0.22,
                                padding: EdgeInsets.symmetric(
                                    vertical: 10, horizontal: 7),
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(8),
                                ),
                                child: Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        SizedBox(
                                          height: 24,
                                          width: 24,
                                          child: SvgPicture.asset(
                                            "assets/icons/bmi.svg",
                                          ),
                                        ),
                                        Text(
                                          "BMI",
                                          style: GoogleFonts.openSans(
                                            color: unaicon,
                                            fontSize: 14,
                                            fontWeight: FontWeight.w400,
                                          ),
                                        ),
                                      ],
                                    ),
                                    Text(
                                      '20',
                                      style: GoogleFonts.openSans(
                                        color: Color(0xFF303841),
                                        fontSize: 18,
                                        fontWeight: FontWeight.w600,
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              Container(
                                height:
                                    MediaQuery.of(context).size.width * 0.20,
                                width: MediaQuery.of(context).size.width * 0.22,
                                padding: EdgeInsets.symmetric(
                                    vertical: 10, horizontal: 7),
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(8),
                                ),
                                child: Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        SizedBox(
                                          height: 24,
                                          width: 24,
                                          child: SvgPicture.asset(
                                            "assets/icons/homea1c.svg",
                                          ),
                                        ),
                                        SizedBox(
                                          width: 5,
                                        ),
                                        Text(
                                          "A1C",
                                          style: GoogleFonts.openSans(
                                            color: unaicon,
                                            fontSize: 14,
                                            fontWeight: FontWeight.w400,
                                          ),
                                        ),
                                      ],
                                    ),
                                    Text(
                                      '7.2%',
                                      style: GoogleFonts.openSans(
                                        color: Color(0xFF303841),
                                        fontSize: 18,
                                        fontWeight: FontWeight.w600,
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              Container(
                                height:
                                    MediaQuery.of(context).size.width * 0.20,
                                width: MediaQuery.of(context).size.width * 0.22,
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(20.0),
                          child: SmallActivebutton(
                            onTap: () {},
                            title: "UPDATE",
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Container(
              decoration: BoxDecoration(
                color: pink,
              ),
              child: ExpansionTile(
                trailing: Icon(
                  Icons.arrow_forward_ios_rounded,
                  color: unaicon,
                ),
                title: Text(
                  "Daily Plan",
                  style: GoogleFonts.openSans(
                    color: unaicon,
                    fontSize: 24,
                    fontWeight: FontWeight.w700,
                  ),
                ),
                expandedCrossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 20,
                      vertical: 5,
                    ),
                    child: Text(
                      'Goals for today :',
                      style: GoogleFonts.openSans(
                        color: Color(0xFF303841),
                        fontSize: 16,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ),
                  Container(
                    color: Colors.white,
                    margin: EdgeInsets.only(
                      left: 8,
                      right: 8,
                      top: 0,
                      bottom: 0.5,
                    ),
                    child: ListTile(
                      leading: SvgPicture.asset("assets/icons/carbs.svg"),
                      title: Text(
                        'Carbs',
                        style: GoogleFonts.openSans(
                          color: Color(0xFF0F1728),
                          fontSize: 16,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      subtitle: Text(
                        '100 g',
                        style: TextStyle(
                          color: Color(0xFF667084),
                          fontSize: 14,
                          fontFamily: 'Open Sans',
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                      trailing: Container(
                        width: 130,
                        height: 60,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Stack(
                              children: [
                                Container(
                                  width: 50,
                                  height: 10,
                                  decoration: BoxDecoration(
                                    color: grey1,
                                    borderRadius: BorderRadius.circular(8),
                                  ),
                                ),
                                Container(
                                  width: 35,
                                  height: 10,
                                  decoration: BoxDecoration(
                                    color: purple,
                                    borderRadius: BorderRadius.circular(8),
                                  ),
                                ),
                              ],
                            ),
                            Text(
                              '70%',
                              style: GoogleFonts.openSans(
                                color: Color(0xFF344053),
                                fontSize: 16,
                                fontWeight: FontWeight.w400,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Container(
                    color: Colors.white,
                    margin: EdgeInsets.only(
                      left: 8,
                      right: 8,
                      top: 0,
                      bottom: 0.5,
                    ),
                    child: ListTile(
                      leading: SvgPicture.asset("assets/icons/protein.svg"),
                      title: Text(
                        'Proteins',
                        style: GoogleFonts.openSans(
                          color: Color(0xFF0F1728),
                          fontSize: 16,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      subtitle: Text(
                        '50 g',
                        style: TextStyle(
                          color: Color(0xFF667084),
                          fontSize: 14,
                          fontFamily: 'Open Sans',
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                      trailing: Container(
                        width: 130,
                        height: 60,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Stack(
                              children: [
                                Container(
                                  width: 50,
                                  height: 10,
                                  decoration: BoxDecoration(
                                    color: grey1,
                                    borderRadius: BorderRadius.circular(8),
                                  ),
                                ),
                                Container(
                                  width: 30,
                                  height: 10,
                                  decoration: BoxDecoration(
                                    color: purple,
                                    borderRadius: BorderRadius.circular(8),
                                  ),
                                ),
                              ],
                            ),
                            Text(
                              '60%',
                              style: GoogleFonts.openSans(
                                color: Color(0xFF344053),
                                fontSize: 16,
                                fontWeight: FontWeight.w400,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Container(
                    color: Colors.white,
                    margin: EdgeInsets.only(
                      left: 8,
                      right: 8,
                      top: 0,
                      bottom: 0.5,
                    ),
                    child: ListTile(
                      leading: SvgPicture.asset("assets/icons/fats.svg"),
                      title: Text(
                        'Fats',
                        style: GoogleFonts.openSans(
                          color: Color(0xFF0F1728),
                          fontSize: 16,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      subtitle: Text(
                        '60 g',
                        style: TextStyle(
                          color: Color(0xFF667084),
                          fontSize: 14,
                          fontFamily: 'Open Sans',
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                      trailing: Container(
                        width: 130,
                        height: 60,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Stack(
                              children: [
                                Container(
                                  width: 50,
                                  height: 10,
                                  decoration: BoxDecoration(
                                    color: grey1,
                                    borderRadius: BorderRadius.circular(8),
                                  ),
                                ),
                                Container(
                                  width: 15,
                                  height: 10,
                                  decoration: BoxDecoration(
                                    color: purple,
                                    borderRadius: BorderRadius.circular(8),
                                  ),
                                ),
                              ],
                            ),
                            Text(
                              '30%',
                              style: GoogleFonts.openSans(
                                color: Color(0xFF344053),
                                fontSize: 16,
                                fontWeight: FontWeight.w400,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Container(
                    color: Colors.white,
                    margin: EdgeInsets.only(
                      left: 8,
                      right: 8,
                      top: 0,
                      bottom: 0.5,
                    ),
                    child: ListTile(
                      leading: SvgPicture.asset("assets/icons/veggies.svg"),
                      title: Text(
                        'Veggies',
                        style: GoogleFonts.openSans(
                          color: Color(0xFF0F1728),
                          fontSize: 16,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      subtitle: Text(
                        '100 g',
                        style: TextStyle(
                          color: Color(0xFF667084),
                          fontSize: 14,
                          fontFamily: 'Open Sans',
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                      trailing: Container(
                        width: 130,
                        height: 60,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Stack(
                              children: [
                                Container(
                                  width: 50,
                                  height: 10,
                                  decoration: BoxDecoration(
                                    color: grey1,
                                    borderRadius: BorderRadius.circular(8),
                                  ),
                                ),
                                Container(
                                  width: 40,
                                  height: 10,
                                  decoration: BoxDecoration(
                                    color: purple,
                                    borderRadius: BorderRadius.circular(8),
                                  ),
                                ),
                              ],
                            ),
                            Text(
                              '80%',
                              style: GoogleFonts.openSans(
                                color: Color(0xFF344053),
                                fontSize: 16,
                                fontWeight: FontWeight.w400,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Container(
                    color: Colors.white,
                    margin: EdgeInsets.only(
                      left: 8,
                      right: 8,
                      top: 0,
                      bottom: 0.5,
                    ),
                    child: ListTile(
                      leading: SvgPicture.asset("assets/icons/workouts.svg"),
                      title: Text(
                        'Workout',
                        style: GoogleFonts.openSans(
                          color: Color(0xFF0F1728),
                          fontSize: 14,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      subtitle: Text(
                        'Exercise name',
                        style: TextStyle(
                          color: Color(0xFF667084),
                          fontSize: 14,
                          fontFamily: 'Open Sans',
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                      trailing: Container(
                        width: 130,
                        height: 60,
                        alignment: Alignment.center,
                        child: Text(
                          '90 Minutes',
                          style: GoogleFonts.openSans(
                            color: Color(0xFF344053),
                            fontSize: 16,
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    color: Colors.white,
                    margin: EdgeInsets.only(
                      left: 8,
                      right: 8,
                      top: 0,
                      bottom: 0.5,
                    ),
                    child: ListTile(
                      leading: SvgPicture.asset("assets/icons/mindfn.svg"),
                      title: Text(
                        'Mindfulness',
                        style: GoogleFonts.openSans(
                          color: Color(0xFF0F1728),
                          fontSize: 16,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      subtitle: Text(
                        'Method name',
                        style: TextStyle(
                          color: Color(0xFF667084),
                          fontSize: 14,
                          fontFamily: 'Open Sans',
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                      trailing: Container(
                        width: 130,
                        height: 60,
                        alignment: Alignment.center,
                        child: Text(
                          '90 Minutes',
                          style: GoogleFonts.openSans(
                            color: Color(0xFF344053),
                            fontSize: 16,
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(8),
                        bottomRight: Radius.circular(8),
                      ),
                    ),
                    margin: EdgeInsets.only(
                      left: 8,
                      right: 8,
                      top: 0,
                      bottom: 5,
                    ),
                    padding: EdgeInsets.all(20),
                    child: SmallActivebutton(
                      title: "UPDATE",
                      onTap: () {},
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Container(
              decoration: BoxDecoration(
                color: blue,
              ),
              child: ExpansionTile(
                title: Text(
                  "Week So Far",
                  style: GoogleFonts.openSans(
                    color: unaicon,
                    fontSize: 24,
                    fontWeight: FontWeight.w700,
                  ),
                ),
                trailing: Icon(
                  Icons.arrow_forward_ios_rounded,
                  color: unaicon,
                ),
                expandedCrossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 8),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(bottom: 8.0, left: 10),
                          child: Text(
                            'Daily Calories',
                            style: GoogleFonts.openSans(
                              color: Color(0xFF303841),
                              fontSize: 18,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ),
                        BarGraph(title: "2000 Cal"),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 24,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 8),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(bottom: 8.0, left: 10),
                          child: Text(
                            'Workout Minutes',
                            style: GoogleFonts.openSans(
                              color: Color(0xFF303841),
                              fontSize: 18,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ),
                        BarGraph(title: "120 m"),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 8,
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Container(
              padding: EdgeInsets.all(5),
              color: orange,
              width: double.infinity,
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(8),
                ),
                child: Column(
                  children: [
                    ExpansionTile(
                      title: Text(
                        "Resources",
                        style: GoogleFonts.openSans(
                          color: unaicon,
                          fontSize: 24,
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                      trailing: Icon(
                        Icons.arrow_forward_ios_rounded,
                        color: unaicon,
                      ),
                      children: [
                        Column(
                          children: List.generate(
                              resources.length,
                              (index) => ListTile(
                                    leading: SizedBox(
                                      height: 32,
                                      width: 32,
                                      child: SvgPicture.asset(
                                        resources[index]["icon"],
                                      ),
                                    ),
                                    title: Text(
                                      resources[index]["title"],
                                    ),
                                    trailing: IconButton(
                                      onPressed: () {},
                                      icon: Icon(
                                        Icons.add,
                                      ),
                                    ),
                                  )
                              // (index) => Container(
                              //   height: 52,
                              //   padding: EdgeInsets.symmetric(
                              //     horizontal: 10,
                              //   ),
                              //   decoration: BoxDecoration(
                              //     color: (index == 3)
                              //         ? Color.fromRGBO(176, 136, 249, 0.1)
                              //         : Colors.white,
                              //     borderRadius: BorderRadius.circular(8),
                              //   ),
                              //   child: Row(
                              //     mainAxisAlignment:
                              //         MainAxisAlignment.spaceBetween,
                              //     children: [
                              //       Row(
                              //         mainAxisAlignment: MainAxisAlignment.start,
                              //         children: [
                              //           SizedBox(
                              //             height: 32,
                              //             width: 32,
                              //             child: SvgPicture.asset(
                              //               resources[index]["icon"],
                              //             ),
                              //           ),
                              //           SizedBox(
                              //             width: 20,
                              //           ),
                              //           Text(
                              //             resources[index]["title"],
                              //           ),
                              //         ],
                              //       ),
                              //       IconButton(
                              //         onPressed: () {},
                              //         icon: Icon(
                              //           Icons.add,
                              //         ),
                              //       ),
                              //     ],
                              //   ),
                              // ),
                              ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    ));
  }
}
