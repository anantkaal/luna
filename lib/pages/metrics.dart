import 'package:flutter/material.dart';
import 'package:luna/model.dart';

class PlanBody extends StatefulWidget {
  const PlanBody({super.key});

  @override
  State<PlanBody> createState() => _PlanBodyState();
}

class _PlanBodyState extends State<PlanBody> {
  @override
  Widget build(BuildContext context) {
    int pagenum = metricpage;
    return Scaffold(
      body: metrices[pagenum]['navigation'],
    );
  }
}
