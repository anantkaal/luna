import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:luna/model.dart';

class CustomToggleButton extends StatefulWidget {
  CustomToggleButton({
    super.key,
    required this.lefttext,
    required this.righttext,
    required this.onChanged,
    required this.initialValue,
  });
  final String lefttext;
  final String righttext;
  final Function(bool) onChanged;
  final bool initialValue;

  @override
  State<CustomToggleButton> createState() => _CustomToggleButtonState();
}

class _CustomToggleButtonState extends State<CustomToggleButton> {
  bool leftselected = true;
  @override
  void initState() {
    super.initState();
    leftselected = widget.initialValue;
  }

  void _handleToggleChange(bool newValue) {
    setState(() {
      leftselected = newValue;
    });
    widget.onChanged(leftselected);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 48,
      width: 120,
      decoration: BoxDecoration(
        color: box1,
        borderRadius: BorderRadius.circular(8),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          GestureDetector(
            onTap: () {
              _handleToggleChange(true);
            },
            child: Container(
              width: 37,
              height: 28,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                color: leftselected ? purple : Colors.transparent,
                borderRadius: BorderRadius.circular(8),
              ),
              child: Text(
                widget.lefttext,
                maxLines: 1,
                textAlign: TextAlign.center,
                style: GoogleFonts.openSans(
                  fontSize: 16,
                  fontWeight: FontWeight.w400,
                  color: leftselected ? Colors.white : unaicon,
                ),
              ),
            ),
          ),
          GestureDetector(
            onTap: () {
              _handleToggleChange(false);
            },
            child: Container(
              width: 37,
              height: 28,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                color: leftselected ? Colors.transparent : purple,
                borderRadius: BorderRadius.circular(8),
              ),
              child: Text(
                widget.righttext,
                textAlign: TextAlign.center,
                maxLines: 1,
                style: GoogleFonts.openSans(
                  fontSize: 16,
                  fontWeight: FontWeight.w400,
                  color: leftselected ? unaicon : Colors.white,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
