import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:luna/model.dart';

class BarGraph extends StatefulWidget {
  BarGraph({super.key, required this.title});
  String title;

  @override
  State<BarGraph> createState() => _BarGraphState();
}

class _BarGraphState extends State<BarGraph> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 250,
      width: double.infinity,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(8),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          SizedBox(
            height: 10,
          ),
          Text(
            widget.title,
            style: GoogleFonts.openSans(
              color: Color(0xFF303841),
              fontSize: 18,
              fontWeight: FontWeight.w600,
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              IconButton(
                onPressed: () {},
                icon: Icon(
                  Icons.chevron_left_sharp,
                ),
              ),
              Text(
                'June 1 - June 7',
                style: GoogleFonts.openSans(
                  color: Color(0xFF303841),
                  fontSize: 16,
                  fontWeight: FontWeight.w400,
                ),
              ),
              IconButton(
                onPressed: () {},
                icon: Icon(
                  Icons.chevron_right_sharp,
                ),
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Column(
                children: [
                  Container(
                    height: 120,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          '3000',
                          style: GoogleFonts.openSans(
                            color: Color(0xFF8D8F91),
                            fontSize: 14,
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                        Text(
                          '2000',
                          style: GoogleFonts.openSans(
                            color: Color(0xFF8D8F91),
                            fontSize: 14,
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                        Text(
                          '1000',
                          style: GoogleFonts.openSans(
                            color: Color(0xFF8D8F91),
                            fontSize: 14,
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                        Text(
                          '0000',
                          style: GoogleFonts.openSans(
                            color: Color(0xFF8D8F91),
                            fontSize: 14,
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 32,
                  ),
                ],
              ),
              Column(
                children: [
                  Stack(
                    alignment: Alignment.bottomCenter,
                    children: [
                      Container(
                        width: 40,
                        height: 140,
                        decoration: BoxDecoration(
                          color: grey1,
                          borderRadius: BorderRadius.circular(50),
                          border: Border.symmetric(
                            horizontal: BorderSide(
                              color: unaicon,
                              width: 10,
                            ),
                            vertical: BorderSide(
                              color: unaicon,
                              width: 15,
                            ),
                          ),
                        ),
                      ),
                      Container(
                        width: 10,
                        height: 80,
                        decoration: BoxDecoration(
                          color: purple,
                          borderRadius: BorderRadius.circular(8),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 12,
                  ),
                  SizedBox(
                    height: 20,
                    child: Text(
                      'Sun',
                      style: GoogleFonts.openSans(
                        color: Color(0xFF8D8F91),
                        fontSize: 14,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                  ),
                ],
              ),
              Column(
                children: [
                  Stack(
                    alignment: Alignment.bottomCenter,
                    children: [
                      Container(
                        width: 10,
                        height: 120,
                        decoration: BoxDecoration(
                          color: grey1,
                          borderRadius: BorderRadius.circular(8),
                        ),
                      ),
                      Container(
                        width: 10,
                        height: 80,
                        decoration: BoxDecoration(
                          color: purple,
                          borderRadius: BorderRadius.circular(8),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 12,
                  ),
                  SizedBox(
                    height: 20,
                    child: Text(
                      'Mon',
                      style: GoogleFonts.openSans(
                        color: Color(0xFF8D8F91),
                        fontSize: 14,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                  ),
                ],
              ),
              Column(
                children: [
                  Stack(
                    alignment: Alignment.bottomCenter,
                    children: [
                      Container(
                        width: 10,
                        height: 120,
                        decoration: BoxDecoration(
                          color: grey1,
                          borderRadius: BorderRadius.circular(8),
                        ),
                      ),
                      Container(
                        width: 10,
                        height: 80,
                        decoration: BoxDecoration(
                          color: purple,
                          borderRadius: BorderRadius.circular(8),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 12,
                  ),
                  SizedBox(
                    height: 20,
                    child: Text(
                      'Tue',
                      style: GoogleFonts.openSans(
                        color: Color(0xFF8D8F91),
                        fontSize: 14,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                  ),
                ],
              ),
              Column(
                children: [
                  Stack(
                    alignment: Alignment.bottomCenter,
                    children: [
                      Container(
                        width: 10,
                        height: 120,
                        decoration: BoxDecoration(
                          color: grey1,
                          borderRadius: BorderRadius.circular(8),
                        ),
                      ),
                      Container(
                        width: 10,
                        height: 80,
                        decoration: BoxDecoration(
                          color: purple,
                          borderRadius: BorderRadius.circular(8),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 12,
                  ),
                  SizedBox(
                    height: 20,
                    child: Text(
                      'Wed',
                      style: GoogleFonts.openSans(
                        color: Color(0xFF8D8F91),
                        fontSize: 14,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                  ),
                ],
              ),
              Column(
                children: [
                  Stack(
                    alignment: Alignment.bottomCenter,
                    children: [
                      Container(
                        width: 10,
                        height: 120,
                        decoration: BoxDecoration(
                          color: grey1,
                          borderRadius: BorderRadius.circular(8),
                        ),
                      ),
                      Container(
                        width: 10,
                        height: 80,
                        decoration: BoxDecoration(
                          color: purple,
                          borderRadius: BorderRadius.circular(8),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 12,
                  ),
                  SizedBox(
                    height: 20,
                    child: Text(
                      'Thu',
                      style: GoogleFonts.openSans(
                        color: Color(0xFF8D8F91),
                        fontSize: 14,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                  ),
                ],
              ),
              Column(
                children: [
                  Stack(
                    alignment: Alignment.bottomCenter,
                    children: [
                      Container(
                        width: 10,
                        height: 120,
                        decoration: BoxDecoration(
                          color: grey1,
                          borderRadius: BorderRadius.circular(8),
                        ),
                      ),
                      Container(
                        width: 10,
                        height: 80,
                        decoration: BoxDecoration(
                          color: purple,
                          borderRadius: BorderRadius.circular(8),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 12,
                  ),
                  SizedBox(
                    height: 20,
                    child: Text(
                      'Fri',
                      style: GoogleFonts.openSans(
                        color: Color(0xFF8D8F91),
                        fontSize: 14,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                  ),
                ],
              ),
              Column(
                children: [
                  Stack(
                    alignment: Alignment.bottomCenter,
                    children: [
                      Container(
                        width: 10,
                        height: 120,
                        decoration: BoxDecoration(
                          color: grey1,
                          borderRadius: BorderRadius.circular(8),
                        ),
                      ),
                      Container(
                        width: 10,
                        height: 80,
                        decoration: BoxDecoration(
                          color: purple,
                          borderRadius: BorderRadius.circular(8),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 12,
                  ),
                  SizedBox(
                    height: 20,
                    child: Text(
                      'Sat',
                      style: GoogleFonts.openSans(
                        color: Color(0xFF8D8F91),
                        fontSize: 14,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }
}
