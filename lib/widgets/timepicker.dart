import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:luna/model.dart';

class TimePicker extends StatefulWidget {
  const TimePicker({
    super.key,
    required this.lefttext,
    required this.righttext,
    required this.onChanged,
    required this.initialValue,
    required this.label,
  });
  final String lefttext;
  final String righttext;
  final Function(bool) onChanged;
  final bool initialValue;
  final String label;

  @override
  State<TimePicker> createState() => _TimePickerState();
}

class _TimePickerState extends State<TimePicker> {
  bool leftselected = true;
  @override
  void initState() {
    super.initState();
    leftselected = widget.initialValue;
  }

  void _handleToggleChange(bool newValue) {
    setState(() {
      leftselected = newValue;
    });
    widget.onChanged(leftselected);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          widget.label,
          style: GoogleFonts.openSans(
            color: unaicon,
            fontSize: 16,
            fontWeight: FontWeight.w400,
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 4.0),
          child: Row(
            children: [
              Container(
                height: 48,
                width: 67,
                decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border.all(
                    color: border,
                  ),
                  borderRadius: BorderRadius.circular(8),
                ),
                child: TextFormField(
                  keyboardType: TextInputType.number,
                  validator: (String? value) {
                    if (value!.isNotEmpty) {
                      final n = int.tryParse(value);
                      if (24 <= n! || n <= 0) {
                        showsnackbar(
                          context,
                          Colors.red,
                          "hours should lie between 0 and 24 ",
                        );
                      } else {
                        return null;
                      }
                    }

                    showsnackbar(
                      context,
                      Colors.red,
                      "hours shouldn't be empty",
                    );
                  },
                  textAlign: TextAlign.center,
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    hintText: "00",
                  ),
                ),
              ),
              SizedBox(
                width: 10,
              ),
              Container(
                height: 48,
                width: 67,
                decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border.all(
                    color: border,
                  ),
                  borderRadius: BorderRadius.circular(8),
                ),
                child: TextFormField(
                  validator: (String? value) {
                    if (value!.isNotEmpty) {
                      final n = int.tryParse(value);
                      if (60 <= n! || n <= 0) {
                        showsnackbar(
                          context,
                          Colors.red,
                          "minutes should lie between 0 and 60",
                        );
                      } else {
                        return null;
                      }
                    }

                    showsnackbar(
                      context,
                      Colors.red,
                      "minutes shouldn't be empty",
                    );
                  },
                  textAlign: TextAlign.center,
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    hintText: "00",
                  ),
                ),
              ),
              SizedBox(
                width: 10,
              ),
              //Toggle switch starts here
              Container(
                height: 48,
                width: 120,
                decoration: BoxDecoration(
                  color: box1,
                  borderRadius: BorderRadius.circular(8),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    GestureDetector(
                      onTap: () {
                        _handleToggleChange(true);
                      },
                      child: Container(
                        width: 37,
                        height: 28,
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                          color: leftselected ? purple : Colors.transparent,
                          borderRadius: BorderRadius.circular(8),
                        ),
                        child: Text(
                          widget.lefttext,
                          maxLines: 1,
                          textAlign: TextAlign.center,
                          style: GoogleFonts.openSans(
                            fontSize: 16,
                            fontWeight: FontWeight.w400,
                            color: leftselected ? Colors.white : unaicon,
                          ),
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        _handleToggleChange(false);
                      },
                      child: Container(
                        width: 37,
                        height: 28,
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                          color: leftselected ? Colors.transparent : purple,
                          borderRadius: BorderRadius.circular(8),
                        ),
                        child: Text(
                          widget.righttext,
                          textAlign: TextAlign.center,
                          maxLines: 1,
                          style: GoogleFonts.openSans(
                            fontSize: 16,
                            fontWeight: FontWeight.w400,
                            color: leftselected ? unaicon : Colors.white,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              //Toggle switch ends here
            ],
          ),
        ),
      ],
    );
  }
}
